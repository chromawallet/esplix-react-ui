import * as React from 'react';
// import * as jsPDF from 'jspdf';

import {Button} from 'semantic-ui-react';
import {ContractInstance} from "esplix";


// PDF generator for contract
//
// Parents:
// App - main component of app
//
// Children:
// 
// Methods:
//
// Props
export interface CustomPDFGeneratorProps {
    contract: ContractInstance; // Current contract
}

// State
export interface CustomPDFGeneratorState {
    val: number;
}

export class CustomPDFGenerator extends React.Component<CustomPDFGeneratorProps, CustomPDFGeneratorState> {
    constructor(props: any) {
        super(props);
        this.state = {
            val: 1
        };
    }


    render() {
        const val = this.state.val;
        const contract = this.props.contract.getFields();
        console.log(contract);

        function downloadPDF() {

            // const doc = new jsPDF({
            //     orientation: 'p'
            // });
            //
            // doc.setFont('Arial');
            // doc.setFontSize(24);
            // doc.text('Mortgage discharge', 40, 30);
            // doc.setFontSize(12);
            // doc.text(`Mortgage discharge for the person identified by the public key \r\t${contract['LAND_REGISTRY_PUB']} \rand the mortgagor ` +
            //     `bank \r\t${contract['REPRESENTATIVE_ROLE_PUB']} \r` +
            //     `for the property \r\t ${contract['']}`, 10, 40);
            // doc.save('Contract.pdf');
        }

        const previousTitleSearch = (
            <a href={`/contracts/title_search_ante.html?` +
            `land-title-ref=${contract['TITLE_REFERENCE']} ` +
            `&mortgage-id=${contract["MORTGAGE_ID"]}` +
            `&bank-pub=${contract["REPRESENTATIVE_ROLE_PUB"]}` +
            `&bank-mortgage=${contract["BANK_OF_THE_MORTGAGE"]}` +
            `&customer-name=${contract["OWNER_FULL_NAME"]}`
            } target='_blank'><Button>Title Search</Button>
            </a>);


        const newTitleSearch = (
            <a href={`/contracts/title_search_post.html?` +
            `land-title-ref=${contract['TITLE_REFERENCE']} ` +
            `&mortgage-id=${contract["MORTGAGE_ID"]}` +
            `&bank-pub=${contract["REPRESENTATIVE_ROLE_PUB"]}` +
            `&bank-mortgage=${contract["BANK_OF_THE_MORTGAGE"]}` +
            `&customer-name=${contract["OWNER_FULL_NAME"]}`
            } target='_blank'><Button>Title Search</Button>
            </a>);

        const cord = (
            <a href={`/contracts/cord.html?` +
            `land-title-ref=${contract['TITLE_REFERENCE']} ` +
            `&mortgage-id=${contract["MORTGAGE_ID"]}` +
            `&bank-pub=${contract["REPRESENTATIVE_ROLE_PUB"]}` +
            `&bank-mortgage=${contract["BANK_OF_THE_MORTGAGE"]}` +
            `&customer-name=${contract["OWNER_FULL_NAME"]}`
            } target='_blank'><Button>CORD</Button>
            </a>);

        const mortgageDischargeDoc = (
            <a href={`/contracts/Discharge_of_Mortgage_dealing.html?` +
            `land-title-ref=${contract['TITLE_REFERENCE']} ` +
            `&mortgage-id=${contract["MORTGAGE_ID"]}` +
            `&bank-pub=${contract["REPRESENTATIVE_ROLE_PUB"]}` +
            `&bank-mortgage=${contract["BANK_OF_THE_MORTGAGE"]}` +
            `&customer-name=${contract["OWNER_FULL_NAME"]}`
            } target='_blank'><Button>Mortgage Discharge</Button>
            </a>);

        return <div>
            {
                contract["CORD_EVIDENCE"] != undefined || contract["CORD_EVIDENCE"] != null ?
                    cord : null
            }
            {
                contract["STATE"] == "MONEY_RECEIVED_LODGMENT_DONE" ?
                    (<span> {newTitleSearch} {mortgageDischargeDoc}  </span>) : contract["BANK_OF_THE_MORTGAGE"] != undefined || contract["BANK_OF_THE_MORTGAGE"] != null ? previousTitleSearch : null
            }
        </div>
    }
}

export default CustomPDFGenerator;
