import ActionCard from "./components/contract/ActionCard";
import ActionCard1 from "./components/contract/ActionCard1";
import * as React from "react";
import {CustomizedContractView} from "./customizer";
import {EDNFileUploader} from "./xfiles";
import {ContractInstance, FieldInfo, EsplixContext, ParameterInfo} from "esplix";
import {AddressBook} from "./defedit";



export const getCustomizer = function(that: any) {

    const addressBook = new AddressBook();

    return {
        addressBook,
        noCustomAddress: true,
        customerEnvironment: {
            customTranslations: {
                tCustom: (sentence: string, ...params: any[]) => {
                    const customTranslate = that.state.customizer.customerEnvironment.customTranslations!;
                    let currentLanguage = that.state.customizer.translate.getLanguage();
                    if(customTranslate.translations[currentLanguage] == null) {
                        console.error("Current language not implemented in customTranslate. Using 'en-en' as default.");
                        currentLanguage = 'en-en';
                    }

                    let translatedSentenceObject = customTranslate.translations[currentLanguage][sentence];
                    let translatedSentence: string;

                    if (translatedSentenceObject === null || translatedSentenceObject === undefined) {
                        // If the sentence does not exist, I just return what I received (Hopefully, a meaningful sentence in English)
                        console.error(sentence, "not existing in translation");
                        translatedSentence = sentence;
                    } else {
                        translatedSentence = translatedSentenceObject.one;
                    }

                    while(translatedSentence.includes('%s')) {
                        if(params.length == 0) {
                            throw "Not enough parameters passed";
                        }
                        const param = params.shift()
                        translatedSentence = translatedSentence.replace('%s', param);
                    }
                    if(params.length > 0) throw "Too many parameters passed";
                    return translatedSentence
                },
                translations: {
                    'en-en': Object.assign(require('./translations/customizer_en-en.json'), require('./translations/test123_en_en.json'), require('./translations/NSWLRS_en-en.json')),
                    'it-bo': Object.assign(require('./translations/customizer_it-bo.json'), require('./translations/test123_it_bo.json'), require('./translations/NSWLRS_it-bo.json'))
                }
            },
            publicPath: null,
            frontTitle: null,
            initialiserRole: null,
            roles: {
                "bank": {
                    value: "bank",
                    text: "Bank",
                    color: "blue",
                    initializer: true
                },
                "broker": {
                    value: "broker",
                    text: "Broker",
                    color: "black",
                    initializer: false
                },
                "land-registry": {
                    value: "land-registry",
                    text: "Land Registry",
                    color: "purple",
                    initializer: true
                },
                "user": {
                    value: "user",
                    text: "User",
                    color: "yellow",
                    initializer: false
                }
            }
        },
        alert: that.showAlert.bind(that),
        getCustomizedContractView(context: EsplixContext, contractInstance: ContractInstance): CustomizedContractView | null {
            if (contractInstance !== null) {
                return {
                    getFieldValue(fieldName: string, fieldInfo: FieldInfo, fieldValue: any): any {
                        if (fieldInfo.type.name === "PUBKEY") {
                            const entry = addressBook.searchAddressByPubkey(fieldValue);
                            if (entry) {
                                return entry.name;
                            } else if (fieldValue)
                                return fieldValue;
                            else return "Not specified";
                        }
                        if (fieldValue === null)
                            return undefined;
                        return fieldValue;
                    },
                    getFieldName(fieldInfo: FieldInfo): any {
                        if (fieldInfo.description)
                            return fieldInfo.description.toLocaleLowerCase();
                        else if (fieldInfo.name)
                            return fieldInfo.name.toLocaleLowerCase();
                        else
                            return "";
                    },
                    getParameterName: (actionName: string | null, parameterInfo: ParameterInfo): string => parameterInfo.name.toLocaleLowerCase(),
                    getActionFormHeader: (actionName: string | null): string => (actionName) ? actionName.toLocaleLowerCase() : "",
                    getActionButtonCaption: (actionName: string | null): string => {
                        return (actionName) ? actionName.toUpperCase() : ""
                    },
                    getCustomActionCardClass: (actionName: string | null, parameterName: string): typeof React.Component | null => {
                        return (actionName && actionName.toUpperCase() === "ADD-FILE") ? ActionCard1 as typeof React.Component : ActionCard as typeof React.Component
                    }
                } as CustomizedContractView
            }
            else
                return {
                    getFieldValue(fieldName: string, fieldInfo: FieldInfo, fieldValue: any): any {
                        return ""
                    },
                    getParameterName: (actionName: string | null, parameterInfo: ParameterInfo): string => {
                        return parameterInfo.name
                    },
                    getActionFormHeader: (actionName: string | null): string => {
                        return "Initial parameters:"
                    },
                    getActionButtonCaption: (actionName: string | null): string => {
                        return "Create contract"
                    },
                    getCustomActionCardClass: (actionName: string | null, parameterName: string): typeof React.Component | null => {
                        return null
                    }
                } as CustomizedContractView
        },
        lookupIDTransform: (s: string) => s,
        async lookup(value: string, context: EsplixContext): Promise<any> {
            const id = this.lookupIDTransform(value);
            const certificates = await context.certificateStore.getCertificates(id!);

            if (certificates.length === 0) {
                this.alert("No certificates were found");

                return certificates;
            }

            addressBook.addAddress(certificates[0]);

            if (certificates.length > 1)
                this.alert("Several certificates with this ID were found, using a random one");

            return certificates;
        },
        fileManager: new EDNFileUploader("http://edn.esplix-r4-1.chromaway.net/api/v0.0.1/content"),
        getRelatedContractInstances(ctx: EsplixContext, inst: ContractInstance): ContractInstance[] {
            return ctx.contractInstanceManager.getContractInstances();
        },
        prettifier(s: string) {
            s = s.toLocaleLowerCase().replace(/_/g, " ");
            return s[0].toUpperCase() + s.slice(1);
        },
        translate: {
            setLanguage: (newLanguage: string) => {
                if (that.state.customizer.translate!.translations[newLanguage] != null) {
                    that.state.customizer.translate!.currentLanguage = newLanguage;
                } else throw "Language not supported";

                window.localStorage.setItem('language', newLanguage);
                that.forceUpdate();
                if (that.state.context && (that.state.context!.principalIdentity.isSetUp() || !that.state.context.principalIdentity.getAttribute('role'))) {
                    that.initContext();
                }
            },
            getTCustom: () => {
                // return t-function for custom words (e.g contract's terms)
                // If this function is not provided, I just return the normal translations
                const customTranslations = that.state.customizer.customerEnvironment.customTranslations;
                if (customTranslations) {
                    // custom translation exists, return the function
                    return customTranslations!.tCustom;
                } else {
                    // custom translation does not exist, return standard t-fucntion
                    return that.state.customizer.translate.t;
                }

            },
            getLanguage: () => that.state.customizer.translate!.currentLanguage,
            currentLanguage: 'it-it',

            t: (sentence: string, ...params: any[]) => {
                const translate = that.state.customizer.translate!;
                let translatedSentence = translate.translations[translate.currentLanguage][sentence];
                if(translatedSentence === null || translatedSentence === undefined) {
                    // If the sentence does not exist, I just return what I received (Hopefully, a meaningful sentence in English)
                    console.error(sentence, "not existing in translation");
                    translatedSentence = sentence
                } else {
                    translatedSentence = translatedSentence.one;
                }

                while(translatedSentence.includes('%s')) {
                    if(params.length == 0) {
                        throw "Not enough parameters passed";
                    }
                    const param = params.shift()
                    translatedSentence = translatedSentence.replace('%s', param);
                }
                if(params.length > 0) throw "Too many parameters passed";
                return translatedSentence
            },
            ts: (sentence: string, ...params: any[]) => {
                const translate = that.state.customizer.translate!;
                let translatedSentence = translate.translations[translate.currentLanguage][sentence];
                if(translatedSentence === null || translatedSentence === undefined) {
                    // If the sentence does not exist, I just return what I received (Hopefully, a meaningful sentence in English)
                    console.error(sentence, "not existing in translation");
                    translatedSentence = sentence
                } else {
                    translatedSentence = translatedSentence.more;
                }

                while(translatedSentence.includes('%s')) {
                    if(params.length == 0) {
                        throw "Not enough parameters passed";
                    }
                    const param = params.shift()
                    translatedSentence = translatedSentence.replace('%s', param);
                }
                if(params.length > 0) throw "Too many parameters passed";
                return translatedSentence
            },

            translations: {
                "en-en": require('./translations/en-en.json'),
                'it-it': require('./translations/it-it.json'),
                'it-bo': require('./translations/it-bo.json')
            }
        },
    };
};

export default getCustomizer;