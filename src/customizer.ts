import {ContractInstance, EsplixContext, FieldInfo, ParameterInfo} from "esplix";
import * as React from "react";
import {AddressBook} from "./defedit";
import { FileManager } from "./filemanager";

import ActionCard from './components/contract/ActionCard';
import ActionCard1 from './components/contract/ActionCard1';

export interface CustomizedContractView {
    getFieldValue(fieldName: string, fieldInfo: FieldInfo, fieldValue: any): any;
    getFieldName(fieldInfo: FieldInfo): any;
    getParameterName(actionName: string | null, parameterInfo: ParameterInfo): string;

    getActionFormHeader(actionName: string | null): string;

    getActionButtonCaption(actionName: string | null): string;

    getCustomActionCardClass(actionName: string | null): typeof React.Component | null;
}

export interface Dictionary {
    one: string,
    more: string
}

export interface Language {
    "Hello": Dictionary,
    "Language": Dictionary,
    "Login": Dictionary,
    "Private key": Dictionary,
    "Wrong private key": Dictionary,
    "Role": Dictionary
}

export interface TranslationLang {
    setLanguage(language: string): void,
    getTCustom(): Function,
    getLanguage(): string,
    currentLanguage: string,

    t(sentence: string, ...params: any[]): string,
    ts(sentence: string, ...params: any[]): string,

    translations: {
        [sentence: string]: Language,
    }
}

export interface CustomTranslation {
    tCustom(sentence: string, ...params: any[]): string,
    translations:{
        [language: string]: {[sentence: string]: { one: string, more: string }}
    }
}

export interface Customizer {
    noCustomAddress: boolean;
    fileManager: FileManager | null;
    addressBook: AddressBook;
    customerEnvironment: {
        customTranslations: CustomTranslation | null;
        publicPath: String | null;
        frontTitle: String | null;
        initialiserRole: String | null;
        roles: {
            [role: string]: {
                value: string,
                text: string,
                color: string,
                initializer: boolean
            };
        };
    };
    alert: (data: any) => null;
    lookupIDTransform(s: string): string;
    lookup(value: string, context: EsplixContext): Promise<any>,
    getCustomizedContractView(context: EsplixContext, contractInstance: ContractInstance | null): CustomizedContractView | null;

    getRelatedContractInstances(context: EsplixContext, contractInstance: ContractInstance): ContractInstance[],

    translate: TranslationLang;

    prettifier(text: String): String;


}

