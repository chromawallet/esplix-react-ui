import {
    ContractDefinition, ContractInstance, EsplixContext, ActionInfo, Parameters, ProcessedMessage, ParameterInfo,
    ActionMatch, RType, MultiSigState, PValue, PValueArray, FieldInfo
} from "esplix/types/index";
import { isArray } from "util";
import {Customizer} from "./customizer";
import { fileSharedPublicly, getShareFilePubKeys } from "./xfiles";

export class ContextManager {
    constructor() {
    }

    static context?: EsplixContext;

    static _init(context: EsplixContext) {
        ContextManager.context = context;
    }

    static createContract(contractDefinition: ContractDefinition, parameters: Parameters): Promise<ContractInstance> {
        return this.context!.contractInstanceManager.createContractInstance(contractDefinition, parameters);
    }

    static getContracts(): ContractInstance[] {
        if (this.context) {
            return this.context.contractInstanceManager.getContractInstances()
        }
        return [];
    }

    static getContractActions(contract: ContractInstance, pubkeys: any): ActionInfo[] {
        return contract.getApplicableActions().map(
            match => contract.getActionInfo(match.name)
        )
    }

    static contractPerformAction(contract: ContractInstance, action: string, params: Parameters): any {
        return contract.performAction(action, params);
    }

    static getContractDefinitions(): ContractDefinition[] {
        if (this.context) {
            return this.context.contractDefinitionManager.getAllDefinitions();
        }

        return [];
    }
}

export class AddressBook {
    addresses: AddressBookEntry[] = [];

    addAddress(ae: AddressBookEntry) {
        this.addresses.push(ae);
    }

    getAddresses(): AddressBookEntry[] {
        return this.addresses;
    }

    searchAddressByPubkey(pubkey: string): AddressBookEntry | null {
        for (let i = 0; i < this.addresses.length; i++)
            if (pubkey === this.addresses[i].pubkey)
                return this.addresses[i];

        return null;
    }
}


export interface AddressBookEntry {
    name: string;
    pubkey: string;
}

export interface EsplixKeyPair { // TODO: move to Esplix
    pubKey: string,
    privKey: string
}


export interface ParameterSimpleProps {
    actionInfo: ActionInfo | null;
    paramInfo: ParameterInfo;
    name: string;
    setParameterValue: (key: string, value: any) => null;
    customizer: Customizer; // Object with settings and service functions for app
}


export interface ParameterCompositeProps extends ParameterSimpleProps {
    context: EsplixContext; // Esplix context of app
    contract: ContractInstance; // Current contract
}


export interface FileSource {
    fieldName: string;
    action: ActionInfo;
}


export class Helper {
    static typeIsArray(type: RType): boolean {
        return (!type.isPrimitive) && (isArray(type.typeSpecifier)) && type.typeSpecifier![0] === "ARRAY";
    }

    static valueIsPubkey(pubkey: string): boolean {
        const pk: Buffer = Buffer.from(pubkey, 'hex');

        if (pk && pk.length===33)
            return true;
        else
            return false;
    };

    static resolvePubkey(pubkey: Buffer, addressBook: AddressBook): string {
        const entry = addressBook.searchAddressByPubkey(pubkey.toString('hex'));

        if (entry)
            return entry.name;
        else
            return pubkey.toString('hex');
    };

    static findRelevantFiles(context: EsplixContext, contractInstance: ContractInstance) {
        const fi = contractInstance.getFieldInfo();
        const fields = contractInstance.getFields();
        const contractDefinition = contractInstance.contractDefinition.r4ContractDefinition;

        const relevantFiles: FileSource[] = [];
        const alreadyAdded: { [key: string]: boolean } = {};

        function addRelevantFile(fieldName: string, action: ActionInfo) {
            if (!alreadyAdded[fieldName]) {
                alreadyAdded[fieldName] = true;
                relevantFiles.push({ fieldName, action });
            }
        }

        const myPubKey = context.principalIdentity.getRawKeyPair().pubKey;

        for (const actionName of Object.keys(contractDefinition.actions)) {
            if (actionName === "$INIT") continue;

            const action = contractDefinition.actions[actionName];
            const hasFiles: string[] = action.update.entries.filter(
                (ue: any) => fi[ue.name].type.name === "FILEHASH"
            ).map((ue: any) => ue.name);

            if (hasFiles.length > 0) {
                const sharedWith = getShareFilePubKeys(contractInstance, action);
                let accessible: boolean = false;

                if (!fileSharedPublicly(contractInstance, action)) {
                    // file is shared privately
                    for (const pub of sharedWith) {
                        if (pub.equals(myPubKey)) {
                            accessible = true;
                        }
                    }
                    // or I am uploader
                    for (const pubk of action.guard.signatures) {
                        if (fields[pubk] === context.principalIdentity.getPublicKey()) {
                            accessible = true;
                        }
                    }
                }
                else {
                    accessible = true; // file is shared publicly
                }
                if (accessible) {
                    hasFiles.forEach((f: string) => addRelevantFile(f, action));
                }
            }

            action.update.entries.forEach(
                (ue: any) => {
                    if (fi[ue.name].type.name === "FILEDATA") {
                        addRelevantFile(ue.name, action);
                    }
                }
            );
        }

        return { relevantFiles }
    }
}
