import {ActionInfo, ContractInstance, EsplixContext, PValue, PValueArray, CryptoSystem, makeAESCryptor}  from "esplix";
import {FileInfo, FileManager} from "./filemanager";
import {isArray} from "util";
import * as WebRequest from "web-request";

export function getAnnotation(tag: string, anns?: PValueArray): PValueArray | null {
    if (!anns) return null; // TODO: bug in ratatosk
    for (let ann of anns) {
        if (isArray(ann)) {
            if (ann![0] === tag) {
                return ann as PValueArray;
            }
        }
    }
    return null
}

export function fileSharedPublicly(contractInstance: ContractInstance, actionInfo: ActionInfo): boolean {
    const shareAnn = (actionInfo && actionInfo.annotations) ? getAnnotation("SHARE-FILE", actionInfo.annotations) : null;
    return shareAnn === null
}

export function getShareFilePubKeys(contractInstance: ContractInstance, actionInfo: ActionInfo): Buffer[] {
    const shareAnn = getAnnotation("SHARE-FILE", actionInfo.annotations);
    if (shareAnn) {
        const fd = shareAnn[2];
        let fields: string[] = [];
        if (isArray(fd)) {
            fields = fd as string[]
        } else {
            fields = [fd as string];
        }
        const fieldValues = contractInstance.getFields();
        return fields.filter( fn => fieldValues[fn] !== null).map(
            fn => Buffer.from(fieldValues[fn], 'hex')
        );
    } else return [];
}

export function deriveSharedFileKey(kind: string, pub: Buffer, priv: Buffer, hash: Buffer, cs: CryptoSystem): Buffer {
    const universalSharedKey = cs.deriveSharedKey(pub, priv);
    return cs.digest(
        Buffer.concat([
            Buffer.from(kind, 'ascii'),
            cs.digest(
                Buffer.concat([
                    universalSharedKey,
                    hash
                ])
            )
        ])
    );
}

export class EDNFileUploader implements FileManager {
    url: string;
    constructor(url: string) {
        this.url = url;
    }

    actuallyUpload(key: string, contents: string) {
        return WebRequest.post(
            this.url,
            {
                json: {
                    keys: [key],
                    content: contents
                }
            }
        ).then( (resp) => {
            if (resp.statusCode !== 200) throw Error("Unsuccessful upload");
            return
        });
    }

    actuallyDownload(key: string): Promise<Buffer> {
        return WebRequest.get(this.url + "/" + key).then(
            (resp) => {
                if (resp.statusCode !== 200) throw Error("Unsuccessful download");
                return Buffer.from(JSON.parse(resp.content).data.content, 'hex');
            }
        )
    }

    downloadSecretFile(context: EsplixContext, contractInstance: ContractInstance, action: ActionInfo, fileHash: Buffer): Promise<Buffer> {
        const cryptoSystem = context.getCryptoSystem();
        const myPrivKey = context.principalIdentity.getRawKeyPair().privKey;
        const senderPubKey = contractInstance.getFields()[ action.guard.signatures[0] ];
        if (senderPubKey === null) return Promise.reject(Error("Unable to download private file: Public key not availbale"));
        const downloadKey = deriveSharedFileKey("upload", Buffer.from(senderPubKey, 'hex'), myPrivKey, fileHash, cryptoSystem);
        const encryptionKey = deriveSharedFileKey("encrypt", Buffer.from(senderPubKey, 'hex'), myPrivKey, fileHash, cryptoSystem);
        return this.actuallyDownload(downloadKey.toString('hex')).then(
            buffer => makeAESCryptor(encryptionKey).decryptor(buffer)
        )
    }

    uploadSecretFile(context: EsplixContext, contractInstance: ContractInstance, actionInfo: ActionInfo, file: FileInfo): Promise<void> {
            const cryptoSystem = context.getCryptoSystem();
            const myPrivKey = context.principalIdentity.getRawKeyPair().privKey;
            const promises : Promise<void>[] = [];
            const pubkeys = getShareFilePubKeys(contractInstance, actionInfo);
            pubkeys.push(context.principalIdentity.getRawKeyPair().pubKey); // TODO: temporary hack
            for (const pubkey of pubkeys) {
                const uploadKey = deriveSharedFileKey("upload", pubkey, myPrivKey, file.hash, cryptoSystem);
                const encryptionKey = deriveSharedFileKey("encrypt", pubkey, myPrivKey, file.hash, cryptoSystem);
                const encryptedContents = makeAESCryptor(encryptionKey).encryptor(file.contents);
                promises.push(this.actuallyUpload(
                    uploadKey.toString('hex'), encryptedContents.toString('hex')
                ));
            }
            return Promise.all(promises).then(() => {return});
    }

    uploadNormalFile(context: EsplixContext, contractInstance: ContractInstance, actionInfo: ActionInfo, file: FileInfo): Promise<void> {
        return this.actuallyUpload(file.hash.toString('hex'),
                file.contents.toString('hex'));
    }

    uploadFile(context: EsplixContext, contractInstance: ContractInstance, actionInfo: ActionInfo, file: FileInfo): Promise<void> {
        if (!fileSharedPublicly(contractInstance, actionInfo)) {
            return this.uploadSecretFile(context, contractInstance, actionInfo, file);
        } else {
            return this.uploadNormalFile(context, contractInstance, actionInfo, file);
        }
    }

    downloadFile(context: EsplixContext, contractInstance: ContractInstance, actionInfo: ActionInfo, fileHash: Buffer): Promise<Buffer> {
        if (fileSharedPublicly(contractInstance, actionInfo)) {
            return this.actuallyDownload(fileHash.toString('hex'));
        } else {
            return this.downloadSecretFile(context, contractInstance, actionInfo, fileHash);
        }
    }

}

