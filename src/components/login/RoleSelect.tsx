import * as React from 'react';
import {EsplixContext} from "esplix";
import {Form, Dropdown} from 'semantic-ui-react';
import {Customizer} from "../../customizer";


// Role select form for login dialog
//
// Parents:
// LoginForm - login user dialog
//
// Children:
//
// Methods:
//
// Props
export interface RoleSelectProps {
    selectedRole: string; // Selected role
    setParameter: (name: string, value: any) => void; // Function of setting parameter
    customizer: Customizer; // Object with settings and service functions for app
    context: EsplixContext; // Esplix context of app
}

// State
export interface RoleSelectState {
}

export class RoleSelect extends React.Component<RoleSelectProps, RoleSelectState> {
    constructor(props: RoleSelectProps) {
        super(props);

        this.state = { };
    };

    onSetRole = (e: Event, data: any) => {
        e.preventDefault();

        const setParameter = this.props.setParameter;

        setParameter("role", data.value as string);
    };

    render() {
        // Keywords: Roles bank user broker land-registry land registry
        const roles = this.props.customizer.customerEnvironment.roles;
        const rolesArr = Object.keys(roles).map(i => roles[i]);

        return (
            <Dropdown placeholder='Role' fluid selection options={rolesArr} onChange={this.onSetRole.bind(this)} />
        )
    }
}

export default RoleSelect;
