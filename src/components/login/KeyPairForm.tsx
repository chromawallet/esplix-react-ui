import * as React from 'react';

import { accessSync } from "fs";

import { EsplixContext } from "esplix";

import { EsplixKeyPair } from '../../defedit';
import { Customizer} from "../../customizer";

import { Button, Input, Label, Form } from 'semantic-ui-react';


// Key-pair form for login dialog
//
// Parents:
// LoginForm - login user dialog
//
// Children:
//
// Methods:
//
// Props
export interface KeyPairFormProps {
    setParameter: (name: string, value: any) => void; // Function of setting parameter
    isWrongPK: boolean; // Attribute - PrivateKey is correct or not
    customizer: Customizer; // Object with settings and service functions for app
    context: EsplixContext; // Esplix context of app
}

// State
export interface KeyPairFormState {
    privateKeyText: string;
    keyPair: EsplixKeyPair | null;
}

export class KeyPairForm extends React.Component<KeyPairFormProps, KeyPairFormState> {
    constructor(props: KeyPairFormProps) {
        super(props);

        this.state = {
            keyPair: null,
            privateKeyText: ""
        };
    };

    async createKeyPair() {
        // Create random private Key
        const setParameter = this.props.setParameter;
        let keyPair: EsplixKeyPair;

        await this.props.context.principalIdentity.generateIdentity();

        keyPair = this.props.context.principalIdentity.getKeyPair();

        this.setState({
            keyPair: keyPair
        });

        setParameter("keyPair", keyPair);
    }

    onChangePrivKeyField(e: MouseEvent) {
        e.preventDefault();

        const setParameter = this.props.setParameter;
        const privateKeyText: string = (e.target as HTMLInputElement).value;

        this.setState({privateKeyText: privateKeyText});

        setParameter("privateKeyText", privateKeyText);
    }
    
    render() {
        const t: Function = this.props.customizer.translate.t;
        const keyPair: EsplixKeyPair | null = this.state.keyPair;
        const privateKeyText: string = this.state.keyPair ? this.state.keyPair.privKey : this.state.privateKeyText;
        const isWrongPK: boolean = this.props.isWrongPK;
        const context: EsplixContext = this.props.context;
        const customizer: Customizer = this.props.customizer;

        return (
            <div>
                <Form.Field style={{ paddingBottom: "5px" }}>
                    <Input label={t("Private key")} className="parameter-primitive"
                           onChange={this.onChangePrivKeyField.bind(this)} value={privateKeyText} />
                    {!keyPair &&
                        <Button icon='add' onClick={this.createKeyPair.bind(this)} />
                    }
                    {isWrongPK === true &&
                        <Label basic color='red' pointing>{t('Wrong private key')}</Label>
                    }
                </Form.Field>
                {isWrongPK !== true &&
                    <div style={{ height: "39px" }}></div>
                }
            </div>
        );
    }
};

export default KeyPairForm;
