import * as React from 'react';
const Component = React.Component;

import { Header, Divider, Button } from 'semantic-ui-react';

import {EsplixContext} from "esplix";

import { Customizer } from '../../customizer';

import KeyPairForm from "./KeyPairForm";
import RoleSelect from "./RoleSelect";


// Login user dialog
//
// Parents:
// App - main component of app
//
// Children:
// KeyPairForm - key-pair form for login dialog
// RoleSelect - dialog with text and Close button
//
// Methods:
//
// Props
export interface LoginFormProps {
    customizer: Customizer; // Object with settings and service functions for app
    context: EsplixContext; // Esplix context of app
}

// State
export interface LoginFormState {
    parameters: any;
    isWrongPK: boolean;
}

export class LoginForm extends React.Component<LoginFormProps, LoginFormState> {
    constructor(props: LoginFormProps) {
        super(props);

        this.state = { parameters: { keyPair: null, privateKeyText: "", role: null }, isWrongPK: false };
    };

    setParameter(name: string, value: any) {
        let parameters: any = this.state.parameters;
        const isWrongPK: boolean = this.state.isWrongPK;

        parameters[name] = value;

        this.setState({ parameters: parameters, isWrongPK: (name === "keyPair" || name === "privateKeyText") ? false : isWrongPK });
    };

    async loginHandler(e: MouseEvent) {
        e.preventDefault();

        let parameters: any = this.state.parameters;
        const showAlert = this.props.customizer.alert;

        if (!parameters.keyPair && parameters.privateKeyText && parameters.role) {
            try {
                await this.props.context.principalIdentity.importIdentityFromPrivateKey(Buffer.from(parameters.privateKeyText, "hex"));
            }
            catch (e) {
                this.setState({isWrongPK: true});

                return;
            }

            parameters.keyPair = this.props.context.principalIdentity.getKeyPair();

            this.setState({ parameters: parameters });
        }

        if (parameters.keyPair && parameters.role) {
            console.log(parameters);
            await this.props.context.principalIdentity.setAttribute("role", parameters.role);

            this.props.customizer.addressBook.addAddress({ name: "me", pubkey: this.props.context.principalIdentity.getPublicKey() })

            showAlert(null);
        }
    };

    render(): any {
        const context: EsplixContext = this.props.context;
        const customizer: Customizer = this.props.customizer;
        const isWrongPK: boolean = this.state.isWrongPK;
        const t: Function = this.props.customizer.translate!.t;

        return (
            <div className="ui tiny modal transition visible active" style={{ marginTop: "-255px", overflow: "visible" }}>
                <Header as='h3'>{t('Login')}</Header>
                <div style={{ padding: "20px 20px 5px 20px" }} className="content">
                    <KeyPairForm context={context} customizer={customizer} isWrongPK={isWrongPK} setParameter={this.setParameter.bind(this)} />
                    <RoleSelect context={context} customizer={customizer} selectedRole={""} setParameter={this.setParameter.bind(this)} />
                </div>
                <Divider />
                <div style={{ textAlign: "right", padding: "0px 20px 0px 20px" }}>
                    <Button type="submit" primary onClick={this.loginHandler.bind(this)}>{t('Login')}</Button>
                </div>
            </div>
        )
    }
}

export default LoginForm;
