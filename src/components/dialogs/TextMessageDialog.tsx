import * as React from 'react';

import { Header, Divider, Button } from 'semantic-ui-react';


// Dialog with text and Close button
//
// Parents:
// Alert - alert dialog window
//
// Children:
// 
// Methods:
//
// Props
export interface TextMessageDialogProps {
    header: string; // Header of dialog window
    text: string; // Text of dialog window
    showAlert: (alertData: any) => null; // Alert dialog opening function
}

// State
export interface TextMessageDialogState {
}

export class TextMessageDialog extends React.Component<TextMessageDialogProps, TextMessageDialogState> {
    constructor(props: TextMessageDialogProps) {
        super(props);

        this.state = {};
    };

    closeHandler(e: MouseEvent) {
        e.preventDefault();
        
        const showAlert: (alertData: any) => null = this.props.showAlert;

        if (showAlert !== null && showAlert !== undefined)
            showAlert(null);
    };

    render() {
        const header = this.props.header;
        const text = this.props.text;

        return (
            <div className="ui tiny modal transition visible active" style={{ marginTop: "-255px" }}>
                <Header as='h3'>{header}</Header>
                <div style={{ padding: "20px 20px 5px 20px" }} className="content">
                    {text}
                </div>
                <Divider />
                <div style={{ textAlign: "right", padding: "0px 20px 0px 20px" }}>
                    <Button secondary onClick={this.closeHandler.bind(this)}>Close</Button>
                </div>
            </div>
        );
    }
}

export default TextMessageDialog;
