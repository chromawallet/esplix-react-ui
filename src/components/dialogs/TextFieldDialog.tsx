import * as React from 'react';

import { Header, Divider, Button, Form } from 'semantic-ui-react';


// Dialog with text field and OK and Close button
//
// Parents:
// Alert - alert dialog window
//
// Children:
// 
// Methods:
//
// Props
export interface TextFieldDialogProps {
    header: string; // Header of dialog window
    fieldname: string; // Text of dialog window
    defaultValue: string; // Initial value of text field
    okHandler?: (value: string) => null; // OK callback handler
    cancelHandler?: () => null; // Cancel callback handler
    showAlert?: (alertData: any) => null; // Alert dialog opening function
}

// State
export interface TextFieldDialogState {
    value: string;
}

export class TextFieldDialog extends React.Component<TextFieldDialogProps, TextFieldDialogState> {
    constructor(props: TextFieldDialogProps) {
        super(props);

        this.state = { value: props.defaultValue };
    };

    setValue(e: KeyboardEvent) {
        e.preventDefault();

        const value: any = (e.target as HTMLInputElement).value;

        this.setState({ value: value });
    };

    okHandler(e: MouseEvent) {
        e.preventDefault();
        
        const okHandler = this.props.okHandler;
        const showAlert = this.props.showAlert;
        const value = this.state.value;

        if (showAlert !== null && showAlert !== undefined)
            showAlert(null);

        if (okHandler)
            okHandler(value);
    };

    cancelHandler(e: MouseEvent) {
        e.preventDefault();
        
        const cancelHandler = this.props.cancelHandler;
        const showAlert = this.props.showAlert;

        if (showAlert !== null && showAlert !== undefined)
            showAlert(null);

        if (cancelHandler)
            cancelHandler();
    };

    render() {
        const header = this.props.header;
        const fieldname = this.props.fieldname;
        const value = this.state.value;

        return (
            <div className="ui tiny modal transition visible active" style={{ marginTop: "-255px" }}>
                <Header as='h3'>{header}</Header>
                <div style={{ padding: "20px 20px 5px 20px" }} className="content">
                    {fieldname}
                    <Form.Input onChange={this.setValue.bind(this)} value={value} style={{width: "100%"}} />
                </div>
                <Divider />
                <div style={{ textAlign: "right", padding: "0px 20px 0px 20px" }}>
                    <Button primary onClick={this.okHandler.bind(this)}>OK</Button>
                    <Button secondary onClick={this.cancelHandler.bind(this)}>Cancel</Button>
                </div>
            </div>
        );
    }
}

export default TextFieldDialog;
