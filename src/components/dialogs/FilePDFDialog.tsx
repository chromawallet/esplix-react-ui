import * as React from 'react';

import PDF from 'react-pdf-js';

import { Button, Pagination } from 'semantic-ui-react';

import { Customizer } from "../../customizer";


// Dialog with PDF data viewer and Close button
//
// Parents:
// FileItem - file card
//
// Children:
// 
// Methods:
//
// Props
export interface FilePDFDialogProps {
    file?: string; // File URL of PDF file
    data?: any; // File data of PDF file
    customizer: Customizer; // Object with settings and service functions for app
}

// State
export interface FilePDFDialogState {
    currentPage: number;
    allPages: number;
}

export class FilePDFDialog extends React.Component<FilePDFDialogProps, FilePDFDialogState> {
    constructor(props: FilePDFDialogProps) {
        super(props);

        this.state = { currentPage: 1, allPages: 0 };
    };

    onPageChange(e: Event, data: any) {
        e.preventDefault();

        this.setState({ currentPage: data.activePage });
    }

    onDocumentComplete(pages: number) {
        this.setState({ currentPage: 1, allPages: pages });
    }

    onDocumentError(e: any) {
        const showAlert = this.props.customizer.alert;

        showAlert("Error displaying PDF document");
    };

    closeHandler(e: MouseEvent) {
        e.preventDefault();

        const showAlert = this.props.customizer.alert;

        showAlert(null);
    };

    render() {
        const currentPage: number = this.state.currentPage;
        const allPages: number = this.state.allPages;
        const file: string | undefined = this.props.file;
        const data: any = this.props.data;

        return (
            <div style={{ maxWidth: "500px", margin: "0 auto" }}>
                {file &&
                    <PDF file={file} fillWidth={true} fillHeight={true} page={currentPage} onDocumentComplete={this.onDocumentComplete.bind(this)} onDocumentError={this.onDocumentError.bind(this)} />
                }
                {data &&
                    <PDF binaryContent={data} fillWidth={true} fillHeight={true} page={currentPage} onDocumentComplete={this.onDocumentComplete.bind(this)} onDocumentError={this.onDocumentError.bind(this)} />
                }
                <br />
                <Pagination defaultActivePage={currentPage} totalPages={allPages} onPageChange={this.onPageChange.bind(this)} />
                <br /><br />
                <Button size={"large"} inverted onClick={this.closeHandler.bind(this)}>Close</Button>
            </div>
        );
    }
};

export default FilePDFDialog;
