import * as React from 'react';

import {  Button } from 'semantic-ui-react';

import {ContractInstance, EsplixContext, FieldInfos, R4ContractDefinition, ActionInfo} from "esplix";
import {Customizer} from "../../customizer";

import FilePDFDialog from "../dialogs/FilePDFDialog";


// File card
//
// Parents:
// ContractDocuments - list of contract documents (files)
//
// Children:
// FilePDFDialog - dialog with PDF data viewer and Close button
//
// Methods:
//
// Props
export interface FileItemProps {
    fileName: string; // File name
    fileURL?: string; // File URL of PDF file
    getFileData: () => Promise<Buffer>; // Function for getting file data of PDF file
    customizer: Customizer; // Object with settings and service functions for app
}

// State
export interface FileItemState {
}


export class FileItem extends React.Component<FileItemProps, FileItemState> {
    constructor(props: FileItemProps) {
        super(props);

        this.state = {};
    };

    openFile() {
        const getFileData = this.props.getFileData;
        const fileURL: string | undefined = this.props.fileURL;
        const customizer: Customizer = this.props.customizer;
        const showAlert = customizer.alert;

        if (getFileData)
            getFileData().then(
                fileData => {
                    showAlert(<FilePDFDialog customizer={customizer} data={fileData} />);
                }
            );
        else if (fileURL)
            showAlert(<FilePDFDialog file={fileURL} customizer={customizer} />);
    };

    render() {
        const fileName: string = this.props.fileName;

        return (
            <div>
                {fileName}: <Button compact color="grey" onClick={this.openFile.bind(this)}>view</Button>
            </div>
        );
    }
};

export default FileItem;
