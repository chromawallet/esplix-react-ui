import * as React from 'react';

import { EsplixContext } from "esplix";

import { Icon, Message, Header, Button } from 'semantic-ui-react';
import {Customizer} from "../../customizer";
import RoleSelect from "../login/RoleSelect";


// User detailed information
//
// Parents:
// App - main component of app
//
// Children:
//
// Methods:
//
// Props
export interface UserCardProps {
    customizer: Customizer;
    context: EsplixContext;
    initContext: () => void;
}

// State
export interface UserCardState {
}

export class UserCard extends React.Component<UserCardProps, UserCardState> {
    constructor(props: UserCardProps) {
        super(props);

        this.state = {};
    };

    logout() {
        const initContext = this.props.initContext;

        initContext();
    };


    render() {
        const customizer: Customizer = this.props.customizer;
        const t = customizer.translate!.t;
        const tCustom = customizer.translate!.getTCustom();
        const context: EsplixContext = this.props.context;
        const isSetUp: boolean = (context) ? context.principalIdentity.isSetUp() : false;
        const pubkey: any = (context && isSetUp === true) ? context.principalIdentity.getPublicKey() : null;

        let role = (context && isSetUp === true) ? context.principalIdentity.getAttribute("role") as string : "";

        return (
            <div>
                {isSetUp &&
                    <Message icon>
                        <Icon name='user circle' size="huge" />
                        <Message.Content style={{ overflow: "auto" }}>
                            <Header as="h3" style={{ display: "inline", paddingRight: "15px" }}>{t('User')}</Header>
                            <Icon link onClick={this.logout.bind(this)} name="user close" />
                            <br />
                            {t('Public key')}:
                            <span style={{ fontWeight: "bold" }} className="chain-id"> {pubkey}</span>
                            <br />
                            {t('Role')}:
                            <span style={{ fontWeight: "bold" }}> {tCustom(role)}</span>
                            <br />
                        </Message.Content>
                    </Message>
                }
            </div>
        );
    }
}

export default UserCard;
