import * as React from 'react';

import './app.css';

import {Segment, Header} from 'semantic-ui-react';

import {EsplixContext} from "esplix";
import {ContractInstance, ContractDefinition, FieldInfo, ParameterInfo} from "esplix";
import {CustomPDFGenerator, CustomPDFGeneratorProps} from "../customPDFGenerator";
import {AddressBook, ContextManager} from "../defedit";

import {CustomizedContractView, Customizer, Dictionary} from '../customizer';
import {DummyFileUploader} from "../filemanager";
import {dummyContext, initializedDummyContext, postchainContext} from "../testcontract";

import ContractNew from './contract/ContractNew';
import ContractsArea from './contract/ContractsArea';
import UserCard from "./user/UserCard";
import LoginForm from "./login/LoginForm";
import Alert from './helpers/Alert';
import {EDNFileUploader} from "../xfiles";
import TopBar from "./helpers/TopBar";
import getCustomizer from "../customizer.example";


// Main component of app
//
// Parents:
//
//
// Children:
// UserCard - user detailed information
// ContractNew - create contract form
// ContractsArea - area of contracts
// LoginForm - login user dialog
// Alert - alert dialog window
//
// Methods:
// showAlert - handler for event of show alert dialog
//
// Props
export interface AppProps {
}

// State
export interface AppState {
    alertData: any; // data for alert window
    context?: EsplixContext; // Esplix context of app
    customizer: Customizer; // Object with settings and service functions for app
}


class App extends React.Component<AppProps, AppState> {
    constructor(props: AppProps) {
        super(props);


        this.state = {
            alertData: null,
            customizer: getCustomizer(this),
        };

        this.getLanguage();
        this.initContext();

        setInterval(() => {
            if (ContextManager.context) {
                if (ContextManager.context.principalIdentity.isSetUp() && ContextManager.context.principalIdentity.getAttribute("role")) {
                    ContextManager.context.update().then(
                        () => {
                            this.forceUpdate()
                        },
                        () => console.log("GOT ERROR IN CONTEXT UPDATE")
                    )
                }
            }
        }, 2500);
    };

    componentDidMount() {
        window["EsplixSimulatorApp"] = this;
    };

    getLanguage() {
        let currentLang: string;
        if (window.localStorage.getItem('language')) {
            currentLang = window.localStorage.getItem('language') as string;
        } else {
            currentLang = 'en-en';
        }
        this.state.customizer.translate.setLanguage(currentLang);
    }

    initContext() {
        const addressBook = this.state.customizer.addressBook;

        postchainContext().then((stuff) => {
            const context: EsplixContext | undefined = stuff.context;
            const customizer = this.state.customizer;
            let alertData: any = null;

            // TODO REMOVE, Here only for testing
            context.principalIdentity.generateIdentity();
            context.principalIdentity.setAttribute("role", 'bank');

            if (context && !context.principalIdentity.isSetUp())
                alertData = <LoginForm context={context} customizer={customizer}/>;

            if (context && context.principalIdentity.isSetUp()) {
                addressBook.addAddress({
                    name: "me",
                    pubkey: context.principalIdentity.getPublicKey()
                })
            }

            this.setState({context: context, customizer: customizer, alertData: alertData});

            ContextManager._init(context);

            this.forceUpdate();
        });
    };

    showAlert(alertData: any) {
        this.setState({alertData: alertData});

        window.setTimeout("$('body').css('overflow', " + ((!alertData) ? "'auto'" : "'hidden'") + ");", 1);
    };

    render() {
        const contracts: ContractInstance[] = ContextManager.getContracts();
        const contractDefinitions: ContractDefinition[] = ContextManager.getContractDefinitions();
        const alertData: any = this.state.alertData;
        const context: EsplixContext | undefined = this.state.context;
        const isSetUp: boolean = (context) ? context.principalIdentity.isSetUp() : false;
        const customizer = this.state.customizer;
        const roles = this.state.customizer.customerEnvironment.roles;

        const keyPairActive = !window.localStorage.getItem('keyPair');

        return (
            <div style={{padding: "3px"}}>
                <TopBar customizer={this.state.customizer}/>

                {context && isSetUp === true && this.state.context!.principalIdentity.getAttribute('role') &&
                <div style={{marginTop: "80px"}}>
                    <UserCard context={context} customizer={customizer}
                              initContext={this.initContext.bind(this)}/>
                    <br/>
                    <ContractNew context={context} contractDefinitions={contractDefinitions}
                                 customizer={customizer}/>
                    <br/>
                    <ContractsArea contracts={contracts} context={context} customizer={customizer}/>
                </div>
                }
                {alertData !== null && alertData !== undefined &&
                <Alert data={alertData} showAlert={this.showAlert.bind(this)}/>
                }
            </div>
        );
    };
}

export default App;
