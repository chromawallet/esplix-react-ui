import * as React from 'react';

import PDF from 'react-pdf-js';

import { List } from 'semantic-ui-react';

import { ContractInstance, EsplixContext, ActionInfo, Parameters, FieldInfos } from "esplix";
import { Customizer, CustomizedContractView } from "../../customizer";
import { FileSource, Helper } from '../../defedit';

import FileItem from "../file/FileItem";
import FilePDFDialog from "../dialogs/FilePDFDialog";


// List of contract documents (files)
//
// Parents:
// ContractCard - contract detailed information
//
// Children:
// FileItem - file card
//
// Methods:
//
// Props
export interface ContractDocumentsProps {
    contract: ContractInstance; // Current contract
    context: EsplixContext; // Esplix context of app
    customizer: Customizer; // Object with settings and service functions for app
}

// State
export interface ContractDocumentsState {
}

export class ContractDocuments extends React.Component<ContractDocumentsProps, ContractDocumentsState> {
    constructor(props: ContractDocumentsProps) {
        super(props);

        this.state = {};
    };

    getFileData(fieldName: string, action: ActionInfo) {
        const contract: ContractInstance = this.props.contract;
        const context: EsplixContext = this.props.context;
        const customizer: Customizer = this.props.customizer;
        const fields = contract.getFields();
        const fieldInfo = contract.getFieldInfo();

        if (fieldInfo[fieldName].type.name === "FILEDATA") {
            return Promise.resolve(Buffer.from(fields[fieldName], "hex"));
        } else {
            if (customizer.fileManager) {
                return customizer.fileManager!.downloadFile(
                    context, contract, action, Buffer.from(fields[fieldName], "hex")
                );
            } else {
                return Promise.reject(Error("Downloads unavailable"))
            }
        }
    };

    render () {
        const contract: ContractInstance = this.props.contract;
        const context: EsplixContext = this.props.context;
        const customizer: Customizer = this.props.customizer;
        const customizedContractView: CustomizedContractView | null = customizer.getCustomizedContractView(context, contract);
        const fields: Parameters = contract.getFields();
        const fieldInfo: FieldInfos = contract.getFieldInfo();
        let { relevantFiles } = Helper.findRelevantFiles(context, contract);

        relevantFiles = relevantFiles.filter(
            (rf: FileSource) => fields[rf.fieldName]
        );

        return <List as='ul'>
            {relevantFiles.map(({ fieldName, action }, index) => {
                const currentFieldInfo = fieldInfo[fieldName];
                let fieldDisplayName = fieldName;

                if (customizedContractView) {
                    fieldDisplayName = customizedContractView.getFieldName(currentFieldInfo);
                }

                return <List.Item as='li' className="chain-id" key={index}>
                    <FileItem fileName={fieldDisplayName} getFileData={this.getFileData.bind(this, fieldName, action)} customizer={customizer} />
                </List.Item>
            })
            }
        </List>
    }
}

export default ContractDocuments;
