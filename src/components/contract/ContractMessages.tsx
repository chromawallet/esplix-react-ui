import * as React from 'react';

import { Segment } from 'semantic-ui-react';

import { EsplixContext, FieldInfos, Parameters, ProcessedMessage } from "esplix";
import { Customizer, CustomizedContractView } from '../../customizer';
import ContractMessage from "./ContractMessage";



// List of contract messages
//
// Parents:
// ContractCard - contract detailed information
//
// Children:
// MessageCard - contract message detailed information
//
// Methods:
//
// Props
export interface ContractMessagesProps {
    messages: ProcessedMessage[];
    customizer: any;
    context: EsplixContext;
    chainID: string;
}

// State
export interface ContractMessagesState {
}

export class ContractMessages extends React.Component<ContractMessagesProps, ContractMessagesState> {
    constructor(props: ContractMessagesProps) {
        super(props);

        this.state = {};
    };

    render() {
        const messages: ProcessedMessage[] = this.props.messages;
        const customizer: Customizer = this.props.customizer;
        const context: EsplixContext = this.props.context;
        const chainID: string = this.props.chainID;

        return (
            <Segment.Group>
                {messages.map((message, index) =>
                    <ContractMessage message={message} customizer={customizer} key={index} context={context} chainID={chainID}/>
                )}
            </Segment.Group>
        );
    };
};

export default ContractMessages;
