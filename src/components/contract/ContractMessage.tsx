import * as React from 'react';

import { EsplixContext, FieldInfos, Parameters, ProcessedMessage } from "esplix";

import {Segment, List, Icon} from 'semantic-ui-react';


// Contract message detailed information
//
// Parents:
// ContractMessages - list of contract messages
//
// Children:
//
// Methods:
//
// Props
export interface ContractMessageProps {
    message: ProcessedMessage;
    customizer: any;
    context: EsplixContext;
    chainID: string;
}

// State
export interface ContractMessageState {
    timestamp: number | null;
    blockHeight: number | null;
    blockRID: string | null;
}

export class ContractMessage extends React.Component<ContractMessageProps, ContractMessageState> {
    constructor(props: ContractMessageProps) {
        super(props);

        this.state = {
            timestamp: null,
            blockHeight: null,
            blockRID: null
        };

        this.props.context.consensusEngine.getBlockInfoByMessageID(props.chainID, props.message.id).then( (r: any) => {
            console.log(" WATCH THIS", r);
            this.setState({
                timestamp: r.timestamp,
                blockHeight: r.blockHeight,
                blockRID: r.blockRID
            });
        });
    };

    render() {
        const message: ProcessedMessage = this.props.message;
        const id = message.id;

        const blockHeight = this.state.blockHeight? this.state.blockHeight : "Not available";
        const timestamp = this.state.timestamp? new Date(this.state.timestamp).toLocaleString() : "Something odd here";

        const action = (message.logicalMessage) ? message.logicalMessage.action : "";
        const resolvePubkey = (pubkey: Buffer): string => {
            const entry = this.props.customizer.addressBook.searchAddressByPubkey(pubkey.toString('hex'));
            if (entry) {
                return entry.name;
            } else {
                return pubkey.toString('hex');
            }
        };

        return (
            <Segment>
                <List as='ul'>
                    <List.Item as='li'><small className="chain-id">{id}</small></List.Item>
                    <List.Item as='li'>Action: {action} {
                        message.success ? <span><Icon name="check" color="green"/></span>
                            : <span><Icon name="cancel" color="red" /> invalid</span>
                    }</List.Item>
                    <List.Item as='li'>Signers: {
                        message.logicalMessage && message.logicalMessage.signedBy.map(
                            (pk, idx) => <span className="chain-id" key={idx}>{resolvePubkey(pk)}</span> )
                    }
                    </List.Item>
                    <List.Item as={"li"}>Block timestamp: <em> { timestamp } </em> (Block height: {blockHeight})</List.Item>
                </List>
            </Segment>
        );
    };
}

export default ContractMessage;