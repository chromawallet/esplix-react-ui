import * as React from 'react';

import {ContractInstance, EsplixContext} from "esplix";

import { Button, Table, Segment } from 'semantic-ui-react';

import { CustomizedContractView, Customizer } from "../../customizer";


// Dashboard card of contract
//
// Parents:
// Dashboard - dashboard of contracts
//
// Children:
//
// Methods:
//
// Props
export interface DashboardCardProps {
    contract: ContractInstance;
    isCompactView: boolean;
    index: number;
    customizer: Customizer;
    context: EsplixContext;
    contractSelector: Function;
}

// State
export interface DashboardCardState {
}

export class DashboardCard extends React.Component<DashboardCardProps, DashboardCardState> {
    constructor(props: DashboardCardProps) {
        super(props);

        this.state = {  };
    };

    getFieldValue(contract: ContractInstance, field: string): String {
        const customizer = this.props.customizer;
        const context = this.props.context;
        const prettify = this.props.customizer.prettifier;

        if(contract.getFields()[field] == context.principalIdentity.getPublicKey()) return "Current user";

        const customizedContractView: CustomizedContractView | null = customizer.getCustomizedContractView(context, contract);

        if (customizedContractView && contract.getFieldInfo()[field]) {
            return customizedContractView.getFieldValue(
                field, contract.getFieldInfo()[field], contract.getFields()[field]
            );
        }
        return contract.getFields()[field]? contract.getFields()[field] : '[n/a]';
    }

    render() {
        const contract = this.props.contract;
        const isCompactView = this.props.isCompactView;
        const index = this.props.index;
        const customizer = this.props.customizer;
        const context = this.props.context;
        const prettify = this.props.customizer.prettifier;

        const normalView = <Table.Row key={index}>
            <Table.Cell>
                {index + 1}
            </Table.Cell>
            <Table.Cell>
                {contract.contractDefinition.r4ContractDefinition.name}
            </Table.Cell>
            <Table.Cell>
                {contract.getApplicableActions().length > 0 ? "YES" : "NO"}
            </Table.Cell>
            <Table.Cell style={{ maxWidth: '100px', 'word-wrap': 'break-word' }}>
                {this.getFieldValue(contract, 'TITLE_REFERENCE') || "[n/a]"}
            </Table.Cell>
            <Table.Cell>
                {contract.getFields().STATE ? this.props.customizer.prettifier(contract.getFields().STATE) : 'To be Initialized'}
            </Table.Cell>
            <Table.Cell style={{ maxWidth: '100px', 'word-wrap': 'break-word' }}>
                {prettify(this.getFieldValue(contract, 'REPRESENTATIVE_ROLE_PUB'))}
            </Table.Cell>
            <Table.Cell style={{ maxWidth: '100px', 'word-wrap': 'break-word' }}>
                {prettify(this.getFieldValue(contract, 'LAND_REGISTRY_PUB'))}
            </Table.Cell>
            <Table.Cell>
                <Button primary onClick={() => this.props.contractSelector(contract)}>
                    Go to contract
                </Button>
            </Table.Cell>
        </Table.Row>;

        const compactView = <Segment>
                <div style={{ display: "inline-block", width: "130px", padding: "10px 10px 10px 10px" }}>Item:</div>
                <div style={{ display: "inline-block", fontWeight: "bold", paddingRight: "10px" }} className="chain-id"> {index + 1}</div>
                <br />
                <div style={{ display: "inline-block", width: "130px", padding: "0px 10px 10px 10px" }}>Contract name:</div>
                <div style={{ display: "inline-block", fontWeight: "bold", paddingRight: "10px" }} className="chain-id"> {contract.contractDefinition.r4ContractDefinition.name}</div>
                <br />
                <div style={{ display: "inline-block", width: "130px", padding: "0px 10px 10px 10px" }}>Require action:</div>
                <div style={{ display: "inline-block", fontWeight: "bold", paddingRight: "10px" }} className="chain-id"> {contract.getApplicableActions().length > 0 ? "YES" : "NO"}</div>
                <br />
                <div style={{ display: "inline-block", width: "130px", padding: "0px 10px 10px 10px" }}>Title reference:</div>
                <div style={{ display: "inline-block", fontWeight: "bold", paddingRight: "10px" }} className="chain-id"> {this.getFieldValue(contract, 'TITLE_REFERENCE') || "[n/a]"}</div>
                <br />
                <div style={{ display: "inline-block", width: "130px", padding: "0px 10px 10px 10px" }}>State:</div>
                <div style={{ display: "inline-block", fontWeight: "bold", paddingRight: "10px" }} className="chain-id"> {contract.getFields().STATE ? prettify(contract.getFields().STATE) : 'To be Initialized'}</div>
                <br />
                <div style={{ display: "inline-block", width: "130px", padding: "0px 10px 10px 10px" }}>Bank:</div>
                <div style={{ display: "inline-block", fontWeight: "bold", paddingRight: "10px" }} className="chain-id"> {prettify(this.getFieldValue(contract, 'REPRESENTATIVE_ROLE_PUB'))}</div>
                <br />
                <div style={{ display: "inline-block", width: "130px", padding: "0px 10px 10px 10px" }}>Land registry:</div>
                <div style={{ display: "inline-block", fontWeight: "bold", paddingRight: "10px" }} className="chain-id"> {prettify(this.getFieldValue(contract, 'LAND_REGISTRY_PUB'))}</div>
                <br />
                <div style={{ padding: "10px" }}>
                    <Button primary onClick={() => this.props.contractSelector(contract)}>
                        Go to contract
                    </Button>
                </div>
        </Segment>;
        
        return (isCompactView) ? compactView : normalView;
    }
}

export default DashboardCard;


