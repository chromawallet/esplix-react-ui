import * as React from 'react';

import { Segment, Header, Message } from 'semantic-ui-react';

import { ActionInfo, Parameters } from "esplix/types/index";
import { ContractInstance, EsplixContext, RType } from "esplix";

import { Customizer, CustomizedContractView } from '../../customizer';

import FormParameters from '../form/FormParameters';
import { ActionCardProps, ActionCardState } from './ActionCard';


// Action form
//
// Parents:
// ActionsList - list of actions
//
// Children:
// FormParameters - form for setting parameters
//
// Methods:
//
// Props
// interface ActionCardProps
//
// State
// interface ActionCardState

export class ActionCard1 extends React.Component<ActionCardProps, ActionCardState> {
    constructor(props: ActionCardProps) {
        super(props);

        this.state = {};
    };

    performAction(params: any) {
        const action: ActionInfo = this.props.action;
        const performAction = this.props.performAction;

        performAction(action.name, params);
    };

    getMultiSigInfo(): { isMultiSig: boolean, actionEnabled: boolean, multiSigMessage: any } {
        const isMultiSig = this.props.contract.getActionSigners(this.props.action.name).length > 1;
        const mss = this.props.contract.multiSigState;

        if (isMultiSig) {

            let messageText = "";
            let actionEnabled = true;
            if (mss.isActive()) {
                if (mss.initiated) {
                    messageText = "Proposal sent, waiting signatures";
                    actionEnabled = false;
                }
                else if (mss.submitted) {
                    messageText = "Signature sent, waiting others";
                    actionEnabled = false;
                } else {
                    messageText = "Received proposal";
                }
            }

            const multiSigMessage = <Message info={true}>
                <Message.Header>Multi-signature action</Message.Header>
                <p>{messageText}</p>
            </Message>;

            return { isMultiSig: true, actionEnabled, multiSigMessage };
        } else {
            return { isMultiSig: false, actionEnabled: true, multiSigMessage: null }
        }
    }

    render() {
        const action: ActionInfo = this.props.action;
        const contract: ContractInstance = this.props.contract;
        const context: EsplixContext = this.props.context;
        const customizer: Customizer = this.props.customizer;
        const customizedContractView: CustomizedContractView | null = this.props.customizedContractView;
        const {
            isMultiSig, actionEnabled, multiSigMessage
        } = this.getMultiSigInfo();


        const actionName = (customizedContractView) ?
            customizedContractView.getActionFormHeader(action.name)
            :
            action.name;

        return (
            <Segment style={{ marginTop: "15px", background: "#e0e0e0" }} color="green" raised>
                <Header as='h3' dividing>{actionName}</Header>
                {isMultiSig && multiSigMessage}
                <FormParameters actionEnabled={actionEnabled} contract={contract} context={context} paramInfos={action.parameters} action={action} formSubmit={this.performAction.bind(this)}
                    customizer={customizer} customizedContractView={customizedContractView} />
            </Segment>
        );
    }
}

export default ActionCard1;
