import * as React from 'react';

import { ActionInfo, Parameters } from "esplix/types/index";

import { Customizer, CustomizedContractView } from '../../customizer';

import ActionCard from './ActionCard';
import {ContractInstance, EsplixContext} from "esplix";

// List of actions
//
// Parents:
// ContractCard - contract detailed information
//
// Children:
// ActionCard - action form
// 
// Methods:
//
// Props
export interface ContractActionsProps {
    actions: ActionInfo[]; // List of contract actions
    performAction: (action: string, params: Parameters) => null; // Function for perform action
    customizer: Customizer; // Object with settings and service functions for app
    customizedContractView: CustomizedContractView | null; // Object with settings and service functions for contract view
    context: EsplixContext; // Esplix context of app
    contract: ContractInstance; // Current contract
}

// State
export interface ContractActionsState {
}

export class ContractActions extends React.Component<ContractActionsProps, ContractActionsState> {
    constructor(props: ContractActionsProps) {
        super(props);

        this.state = {};
    };

    render() {
        const actions: ActionInfo[] = this.props.actions;
        const performAction = this.props.performAction;
        const context: EsplixContext = this.props.context;
        const customizer: Customizer = this.props.customizer;
        const customizedContractView: CustomizedContractView | null = this.props.customizedContractView;

        return (
            <div>
                {actions.map((action) => {
                    let Card: typeof React.Component | null = (customizedContractView) ? customizedContractView.getCustomActionCardClass(action.name) : ActionCard as typeof React.Component;

                    if (Card === null)
                        Card = ActionCard as typeof React.Component;

                    return (
                        <Card contract={this.props.contract} context={context} action={action} performAction={performAction} customizer={customizer} customizedContractView={customizedContractView}
                              key={action.name} />
                    )
                }
                )}
            </div>
        );
    }
}

export default ContractActions;
