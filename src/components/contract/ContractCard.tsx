import * as React from 'react';

import { Segment, Header, Grid, Icon, Message, List, Button, SegmentProps, SemanticCOLORS } from 'semantic-ui-react';

import {ContractInstance, ActionInfo, Parameters} from "esplix";
import {EsplixContext} from "esplix";

import { ContextManager, Helper, AddressBook } from '../../defedit';
import { Customizer, CustomizedContractView} from "../../customizer";
import { CustomPDFGenerator } from './../../customPDFGenerator'

import ContractActions from './ContractActions';
import ContractFields from "./ContractFields";
import ContractMessages from "./ContractMessages";
import ContractDocuments from "./ContractDocuments";


// Contract detailed information
//
// Parents:
// ContractsArea - area of contracts
//
// Children:
// ContractActions - list of actions
// ContractFields - list of contract fields
// ContractMessages - list of contract messages
// ContractDocuments - list of contract documents (files)
//
// Methods:
//
// Props
export interface ContractCardProps {
    contract: ContractInstance; // Current contract
    index: number; // Index (number) of card
    openDashboard: Function; // Dashboard showing function
    customizer: Customizer; // Object with settings and service functions for app
    context: EsplixContext; // Esplix context of app
    isCompactView?: boolean; // Attribute - compact or normal view of contract card
}

// State
export interface ContractCardState {
    contractVisible: boolean;
    contentVisible: boolean;
    actionsVisible: boolean;
    messageChainVisible: boolean;
    docsVisible: boolean;
    fieldsVisible: boolean;
}

export class ContractCard extends React.Component<ContractCardProps, ContractCardState> {
    constructor(props: ContractCardProps) {
        super(props);

        this.state = {
            contractVisible: true,
            contentVisible: true,
            actionsVisible: true,
            messageChainVisible: true,
            docsVisible: true,
            fieldsVisible: true
        };

    };

    hideSegment (tag: string) {
        const hideState = {};

        hideState[tag + "Visible"] = false;

        this.setState(hideState);
    };


    showSegment (tag: string) {
        const showState = {};

        showState[tag + "Visible"] = true;

        this.setState(showState);
    };

    renderCollapsibleSegment (tag: string, header: string, body: any) {
        if (this.state[tag + "Visible"]) {
            return <Segment key={tag}>
                <Header as="h3" onClick={this.hideSegment.bind(this, tag)} style={{ display: "inline", paddingRight: "15px" }}>{header}</Header>
                <Icon link name="chevron up" size="small" onClick={this.hideSegment.bind(this, tag)} />
                {body()}
            </Segment>
        }
        else {
            return <Segment key={tag}>
                <Header as="h3" onClick={this.showSegment.bind(this, tag)} style={{ display: "inline", paddingRight: "15px" }}>{header}</Header>
                <Icon link name="chevron down" size="small" onClick={this.showSegment.bind(this, tag)} />
            </Segment>
        }
    };

    performAction(action: string, params: Parameters) {
        const contract: ContractInstance = this.props.contract;
        const showAlert = this.props.customizer.alert;

        console.log(JSON.stringify(params));

        ContextManager.contractPerformAction(contract, action, params).then(
            () => showAlert("Action was performed successfully & included into the blockchain"),
            () => showAlert("Error performing action")
        );
    };

    openDashboard(e: Event) {
        e.preventDefault();

        const openDashboard: Function = this.props.openDashboard;

        openDashboard();
    };

    render() {
        const contract: ContractInstance = this.props.contract;
        const contractVisible: boolean = this.state.contractVisible;
        const displayContract: string = (contractVisible === true) ? "block" : "none";
        const contentVisible: boolean = this.state.contentVisible;
        const displayContent: string = (contentVisible === true) ? "block" : "none";
        const index: number = this.props.index;
        const actions: ActionInfo[] = ContextManager.getContractActions(contract, {});
        const context: EsplixContext = this.props.context;
        const customizer: Customizer = this.props.customizer;
        const customizedContractView: CustomizedContractView | null = customizer.getCustomizedContractView(context, contract);
        const children = this.props.children;
        const addresBook: AddressBook = this.props.customizer.addressBook;
        const isCompactView: boolean | undefined = this.props.isCompactView;

        const header = <Header as='h2'>
            Esplix chainID: <span className="chain-id" style={{ fontSize: "smaller" }}>{contract.getChainID()}</span>
        </Header>;

        const normalView = <Segment style={{ display: displayContract, marginBottom: "35px" }}>
                {header}
                <div style={{ paddingTop: "20px", display: displayContent }}>
                    <Grid divided>
                        <Grid.Column width="4">
                            <Header as='h4'>Fields</Header>
                            <ContractFields fields={contract.getFields()} fieldInfo={contract.getFieldInfo()}
                                customizedContractView={customizedContractView}
                                customizer={customizer} />
                            <ContractDocuments contract={contract} customizer={customizer} context={context} />
                        </Grid.Column>
                        <Grid.Column width="8">
                            <Header as='h4'>Actions</Header>
                            <ContractActions contract={contract} context={context} actions={actions}
                                performAction={this.performAction.bind(this)} customizer={customizer}
                                customizedContractView={customizedContractView} />
                        </Grid.Column>
                        <Grid.Column width="4">
                            <Header as='h4'>Message chain</Header>
                            <ContractMessages messages={contract.getMessageChain()} customizer={customizer} chainID={contract.getChainID()} context={context} />
                        </Grid.Column>
                    </Grid>
                    <br />
                    {children}
                </div>
                <div style={{ padding: "10px" }}>
                    <a href="/" onClick={this.openDashboard.bind(this)} style={{ color: "rgba(0, 0, 0, 0.87)" }}><Icon link name="arrow left" size="small" /> Back to dashboard</a>
                </div>
                {
                    //<div style={{ position: "absolute", top: "0px", right: "0px" }}>
                    //    {contentVisible
                    //        ? <Icon link name="chevron up" size="small" onClick={this.hideSegment.bind(this, "content")} />
                    //        : <Icon link name="chevron down" size="small" onClick={this.showSegment.bind(this, "content")} />
                    //    }
                    //    <Icon link name="close" size="small" onClick={this.hideSegment.bind(this, "contract")} />
                    //</div>
                }
            </Segment>;

        const compactView =
            <Segment.Group stacked style={{ display: displayContract, marginBottom: "35px" }}>
                <Segment>
                    {header}
                    <CustomPDFGenerator contract={contract}/>
                </Segment>
                <div style={{ display: displayContent }}>
                    {
                        this.renderCollapsibleSegment("actions",
                            actions.length > 0 ? "Available actions" : "No actions available",
                            () => {
                                if (actions.length > 0) {
                                    return <ContractActions contract={contract} context={context} actions={actions}
                                        performAction={this.performAction.bind(this)}
                                        customizer={customizer}
                                        customizedContractView={customizedContractView} />
                                } else {
                                    const allApplicableActions = contract.getAllApplicableActions();

                                    if (allApplicableActions.length > 0) {
                                        return <Message info>
                                            <Message.Header>Waiting others to perform an action</Message.Header>
                                            <List as="ul">
                                                {
                                                    // TODO: describe signers
                                                    allApplicableActions.map(am => {
                                                        const action = contract.getActionInfo(am.name);
                                                        return <List.Item as="li" key={am.name}>
                                                            <span style={{ fontWeight: "bold" }}>{action.description || action.name}</span>, by
                                                            {
                                                                am.matchingPubKeys.map((pk, index) => {
                                                                    const nameOrPK = Helper.resolvePubkey(pk, addresBook);

                                                                    return <span className={"chain-id"} key={index}> {nameOrPK} </span>
                                                                })
                                                            }
                                                        </List.Item>;
                                                    })
                                                }
                                            </List>
                                        </Message>
                                    } else return null
                                }
                            }
                        )
                    }
                    {
                        this.renderCollapsibleSegment("fields",
                            "Data fields",
                            () => <ContractFields fields={contract.getFields()} fieldInfo={contract.getFieldInfo()}
                                customizedContractView={customizedContractView}
                                customizer={customizer} />)
                    }
                    {
                        this.renderCollapsibleSegment("docs",
                            "Documents",
                            () => <ContractDocuments contract={contract} customizer={customizer} context={context} />
                        )
                    }
                    {
                        this.renderCollapsibleSegment("messageChain",
                            "Message chain",
                            () => <ContractMessages messages={contract.getMessageChain()} customizer={customizer} chainID={contract.getChainID()} context={context} />
                        )
                    }
                    {children}
                </div>
                <div style={{ padding: "10px" }}>
                    <a href="/" onClick={this.openDashboard.bind(this)} style={{ color: "rgba(0, 0, 0, 0.87)" }}><Icon link name="arrow left" size="small" /> Back to dashboard</a>
                </div>
                {
                    //<div>
                    //    {contentVisible
                    //        ? <Icon link name="chevron up" size="small" onClick={this.hideSegment.bind(this, "content")} />
                    //        : <Icon link name="chevron down" size="small" onClick={this.showSegment.bind(this, "content")} />
                    //    }
                    //    <Icon link name="close" size="small" onClick={this.hideSegment.bind(this, "contract")} />
                    //</div>
                }
            </Segment.Group>;

        return isCompactView ? compactView : normalView;
    }
}

export default ContractCard;
