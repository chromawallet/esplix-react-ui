import * as React from 'react';

import { Header, Segment, Icon, Message } from 'semantic-ui-react';

import { ContractInstance } from "esplix/types/index";
import {EsplixContext} from "esplix";

import { Customizer } from "../../customizer";

import ContractCard from './ContractCard';
import Dashboard from './Dashboard';


// Area of contracts
//
// Parents:
// App - main component of app
//
// Children:
// ContractCard - contract detailed information
// Dashboard - dashboard of contracts
//
// Methods:
//
// Props
export interface ContractsAreaProps {
    contracts: ContractInstance[]; // List of contracts
    customizer: Customizer; // Object with settings and service functions for app
    context: EsplixContext; // Esplix context of app
}

// State
export interface ContractsAreaState {
    showDashboard: Boolean;
    toggleDashboard: Boolean;
    selectedContract: ContractInstance | null;
}

export class ContractsArea extends React.Component<ContractsAreaProps, ContractsAreaState> {
    constructor(props: ContractsAreaProps) {
        super(props);

        this.state = {
            showDashboard: true,
            toggleDashboard: true,
            selectedContract: null
        };

        this.toggleDashboard = this.toggleDashboard.bind(this);

        this.selectContract = this.selectContract.bind(this);
    };

    toggleDashboard() {
        this.setState({ toggleDashboard: !this.state.toggleDashboard });

        console.log(this.state.toggleDashboard)
    }

    selectContract(contract: ContractInstance) {
        this.setState({ selectedContract: contract, showDashboard: false });

        //this.toggleDashboard();
    }

    openDashboard(contract: ContractInstance) {
        this.setState({ selectedContract: null, showDashboard: true });

        //this.toggleDashboard();
    }

    dashboard() {
        return <Segment>
            <Header as="h3" style={{ display: "inline", paddingRight: "15px" }}>Dashboard</Header>
            {this.state.toggleDashboard === true &&
                <Icon link onClick={this.toggleDashboard} name="chevron up" size="small" />
            }
            {this.state.toggleDashboard !== true &&
                <Icon link onClick={this.toggleDashboard} name="chevron down" size="small" />
            }
            {this.state.toggleDashboard &&
                <div style={{ paddingTop: "15px" }}>
                    <Dashboard contracts={this.props.contracts} contractSelector={this.selectContract} customizer={this.props.customizer} context={this.props.context} />
                </div>
            }
        </Segment>
    }

    noContractsInDashboard() {
        return <Message>
            No contracts in the dashboard
        </Message>
    }

    render() {
        const context: EsplixContext = this.props.context;
        const customizer: Customizer = this.props.customizer;

        return (
            <div>
                {this.state.showDashboard && this.props.contracts.length > 0 &&
                    this.dashboard()
                }
                {this.state.showDashboard && this.props.contracts.length === 0 &&
                    this.noContractsInDashboard()
                }
                {this.state.showDashboard === false && this.state.selectedContract &&
                    <ContractCard isCompactView={true} contract={this.state.selectedContract} openDashboard={this.openDashboard.bind(this)} index={0} customizer={customizer} context={context} />
                }
            </div>
        );
    }
}

export default ContractsArea;
