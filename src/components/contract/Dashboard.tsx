import * as React from 'react';

import {ContractInstance, EsplixContext} from "esplix";

import { Table } from 'semantic-ui-react';

import {CustomizedContractView, Customizer} from "../../customizer";

import DashboardCard from './DashboardCard';


// Dashboard of contracts
//
// Parents:
// ContractsArea - area of contracts
//
// Children:
// DashboardCard - dashboard card of contract
//
// Methods:
//
// Props
export interface DashboardProps {
    contracts: ContractInstance[];
    customizer: Customizer;
    context: EsplixContext;
    contractSelector: Function;
}

// State
export interface DashboardState {
    isCompactView: boolean;
}

export class Dashboard extends React.Component<DashboardProps, DashboardState> {
    constructor(props: DashboardProps) {
        super(props);

        this.state = { isCompactView: false};
    };

    componentDidMount() {
        this.setState({ isCompactView: (window.innerWidth <= 767) ? true : false });
    };

    render() {
        const customizer = this.props.customizer;
        const context = this.props.context;
        const contractSelector = this.props.contractSelector;
        const isCompactView = this.state.isCompactView;
        
        const normalView = <Table celled selectable sortable>
            <Table.Header>
                <Table.Row>
                    <Table.HeaderCell> Item </Table.HeaderCell>
                    <Table.HeaderCell> Contract name </Table.HeaderCell>
                    <Table.HeaderCell> Require action </Table.HeaderCell>
                    <Table.HeaderCell> Title reference </Table.HeaderCell>
                    <Table.HeaderCell> State </Table.HeaderCell>
                    <Table.HeaderCell> Bank </Table.HeaderCell>
                    <Table.HeaderCell> Land registry </Table.HeaderCell>
                    <Table.HeaderCell> Contract </Table.HeaderCell>
                </Table.Row>
            </Table.Header>

            <Table.Body>
                {this.props.contracts.map((contract, index) => {
                    return <DashboardCard contract={contract} isCompactView={false} index={index} contractSelector={contractSelector} customizer={customizer} context={context} />;
                })
                }
            </Table.Body>
        </Table>;
        
        const compactView = <div>
                {this.props.contracts.map((contract, index) => {
                    return <DashboardCard contract={contract} isCompactView={true} index={index} contractSelector={contractSelector} customizer={customizer} context={context} />;
                })
                }
        </div>;
        
        return (isCompactView) ? compactView : normalView;
    }
}

export default Dashboard;

