import * as React from 'react';

import { Segment, Header, Dropdown, Icon } from 'semantic-ui-react';

import { ContractDefinition, ParameterInfo, Parameters, R4ContractDefinition } from "esplix/types/index";
import {EsplixContext} from "esplix";

import { ContextManager } from "../../defedit";
import { Customizer, CustomizedContractView } from "../../customizer";

import FormParameters from '../form/FormParameters';


// Create contract form
//
// Parents:
// App - main component of app
//
// Children:
// FormParameters - form for setting parameters
// 
// Methods:
//
// Props
export interface ContractNewProps {
    contractDefinitions: ContractDefinition[]; // List of contract definitions
    customizer: Customizer; // Object with settings and service functions for app
    context: EsplixContext; // Esplix context of app
}

// State
export interface ContractNewState {
    selectedDefinition: string;
    isShowForm: boolean;
}

export class ContractNew extends React.Component<ContractNewProps, ContractNewState> {
    constructor(props: ContractNewProps) {
        super(props);

        this.state = { selectedDefinition: "", isShowForm: true };
    };

    setDefinition(e: Event, data: any) {
        e.preventDefault();

        this.setState({ selectedDefinition: data.value });
    };

    hideForm(e: Event) {
        e.preventDefault();

        this.setState({ isShowForm: false });
    };

    showForm(e: Event) {
        e.preventDefault();

        this.setState({ isShowForm: true });
    };

    createContract(params: Parameters) {
        const contractDefinitions: ContractDefinition[] = this.props.contractDefinitions;
        const selectedDefinition: string = this.state.selectedDefinition;
        let currentDefinition: ContractDefinition | null = null;
        const showAlert = this.props.customizer.alert;

        console.log(JSON.stringify(params));

        for (let i = 0; i < contractDefinitions.length; i++)
            if (selectedDefinition === contractDefinitions[i].contractHash) {
                currentDefinition = contractDefinitions[i];

                break;
            }

        if (currentDefinition) {
            this.props.context.contractInstanceManager.createContractInstance(currentDefinition, params).then(
                () => {
                    showAlert("Contract created successfully");
                    this.setState({ selectedDefinition: "", isShowForm: false });
                },
                () => showAlert("Error creating contract")

            );
        }
    };

    render() {
        const t: Function = this.props.customizer.translate.t;
        const contractDefinitions: ContractDefinition[] = this.props.contractDefinitions;
        const selectedDefinition: string = this.state.selectedDefinition;
        const isShowForm: boolean = this.state.isShowForm;
        const styleDisplay: string = (isShowForm === true) ? "block" : "none";
        const context: EsplixContext = this.props.context;
        const customizer: Customizer = this.props.customizer;
        const customizedContractView: CustomizedContractView | null = customizer.getCustomizedContractView(context, null);
        let currentDefinition: R4ContractDefinition | null = null;
        let options = [];
        let parameterInfos: ParameterInfo[] = [];

        for (let i = 0; i < contractDefinitions.length; i++) {
            options.push({ key: contractDefinitions[i].contractHash, value: contractDefinitions[i].contractHash, text: contractDefinitions[i].r4ContractDefinition.name });

            if (selectedDefinition === contractDefinitions[i].contractHash) {
                currentDefinition = contractDefinitions[i].r4ContractDefinition;

                for (let i = 0; i < currentDefinition.fieldInfo.length; i++)
                    if (currentDefinition.fieldInfo[i].init === undefined)
                        parameterInfos.push(currentDefinition.fieldInfo[i]);
            }
        }

        return (
            <Segment>
                <Header as='h3' style={{ display: "inline", paddingRight: "15px" }}>{t('Create new contract')}</Header>
                {isShowForm===true &&
                    <Icon link onClick={this.hideForm.bind(this)} name="chevron up" size="small" />
                }
                {isShowForm !== true &&
                    <Icon link onClick={this.showForm.bind(this)} name="chevron down" size="small" />
                }
                <div style={{ paddingTop: "20px", display: styleDisplay }}>
                    <Dropdown placeholder={t('Select PublicKey value')} selection fluid options={options} value={selectedDefinition} onChange={this.setDefinition.bind(this)} />
                    <br />
                    {currentDefinition &&
                        <FormParameters actionEnabled={true} contract={null} context={context} paramInfos={parameterInfos} action={null} formSubmit={this.createContract.bind(this)}
                            customizer={customizer} customizedContractView={customizedContractView} />
                    }
                </div>
            </Segment>
        );
    }
}


export default ContractNew;
