import * as React from 'react';

import {FieldInfos, Parameters} from "esplix";

import { List } from 'semantic-ui-react';
import {Customizer, CustomizedContractView} from "../../customizer";


// List of contract fields
//
// Parents:
// ContractCard - contract detailed information
//
// Children:
// 
// Methods:
//
// Props
export interface ContractFieldsProps {
    fields: Parameters; // List of contract fields
    fieldInfo: FieldInfos; // Current field
    customizer: Customizer; // Object with settings and service functions for app
    customizedContractView: CustomizedContractView | null; // Object with settings and service functions for contract view
}

// State
export interface ContractFieldsState {
}

export class ContractFields extends React.Component<ContractFieldsProps, ContractFieldsState> {
    constructor(props: ContractFieldsProps) {
        super(props);

        this.state = {};
    };

    render() {
        const prettifier = this.props.customizer.prettifier;
        const fields: Parameters = this.props.fields;
        const fieldInfo: FieldInfos = this.props.fieldInfo;
        const customizedContractView: CustomizedContractView | null = this.props.customizedContractView;

        return (
            <List as='ul'>
                {Object.keys(fields).map((fieldName, index) => {
                    const fieldValue = fields[fieldName];
                    const currentFieldInfo = fieldInfo[fieldName];
                    let fieldDisplay = "nil";

                    if (customizedContractView) {
                        fieldDisplay = customizedContractView.getFieldValue(fieldName, currentFieldInfo, fieldValue);
                    }
                    else if (fieldValue) {
                        if (typeof fieldValue === "string") {
                            if (fieldValue.length > 125)
                                fieldDisplay = fieldValue.substr(0, 100) + "...";
                            else
                                fieldDisplay = fieldValue;
                        }
                        else
                            fieldDisplay = fieldValue.toString();
                    }

                    if (fieldDisplay === undefined)
                        return null;

                    let fieldDisplayName = fieldName;

                    if (customizedContractView) {
                        fieldDisplayName = customizedContractView.getFieldName(currentFieldInfo);
                    }

                    return <List.Item as='li' className="chain-id" key={index}>{prettifier(fieldDisplayName)}: { fieldDisplay} </List.Item>
                })
                }
            </List>
        );
    };
};

export default ContractFields;
