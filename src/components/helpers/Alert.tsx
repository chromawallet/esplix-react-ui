import * as React from 'react';

import { TextMessageDialog } from '../dialogs/TextMessageDialog';


// Alert dialog window
//
// Parents:
// App - main component of app
//
// Children:
// TextMessageDialog - dialog with text and Close button
// data - content of dialog window
//
// Methods:
// setHeightTop - set height and top of alert
//
// Props
export interface AlertProps {
    data: any; // Content of dialog window
    showAlert: (alertData: any) => null; // Alert dialog opening function
}

// State
export interface AlertState {
}

export class Alert extends React.Component<AlertProps, AlertState> {
    constructor(props: AlertProps) {
        super(props);

        this.state = {};
    };

    componentDidMount() {
        this.setHeightTop();
    };

    componentDidUpdate() {
        this.setHeightTop();
    };

    setHeightTop() {
        window.setTimeout("var height = $(window).height(); var height1 = $('.ui.modal').children('.content').height() + 25 + 65 + 60 + 10;"
            + " var height2 = Math.min(height * 0.9, height1); $('.ui.modal').children('.content').height(height2 - (25 + 65 + 60 + 10));"
            + " $('.ui.modal').height(height2); $('.ui.modal').css('margin-top', -1 * height2 / 2);", 1);
    };

    render() {
        const showAlert: (alertData: any) => null = this.props.showAlert;
        let data = this.props.data;

        if (typeof data === "string")
            data = <TextMessageDialog header="Information" text={data} showAlert={showAlert} />

        return (
            <div className="ui page modals dimmer transition visible active">
                {data}
            </div>
        );
    }
}

export default Alert;
