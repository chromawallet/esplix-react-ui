import * as React from 'react';

import {Dropdown, DropdownProps, Image, Segment} from 'semantic-ui-react';
import {Customizer} from "../../customizer";
import {SyntheticEvent} from "react";

export interface TopBarProps {
    customizer: Customizer
}

export interface TopBarState {
}

export class TopBar extends React.Component<TopBarProps, TopBarState> {
    constructor(props: TopBarProps) {
        super(props);
        this.state = {}
    }

    render() {

        const t = this.props.customizer.translate!.t;
        const translator = this.props.customizer.translate!;
        // TODO: Put this is customizer
        const languageOptions = [
            { key: 'it-bo', text: 'Bolognese', value: 'it-bo' },
            { key: 'it-it', text: 'Italian', value: 'it-it' },
            { key: 'en-en', text: 'UK English', value: 'en-en' },
            { key: 'en-us', text: 'US English', value: 'en-us' },
        ];


        return <div
            style={{zIndex: 1001, position: "absolute", width: "100%", top: 0, right: 0, left: 0}}>
            <Segment.Group horizontal compact={true} style={{borderRadius: 0, margin: 0, top: 0}}>
                <Segment style={{display: 'flex'}}>
                    <div style={{padding:"0 20px"}}>
                        <Image src='/img/Esplix-Icon-Color-High.png' size={'mini'}/>
                    </div>
                    <div style={{marginLeft: '1em'}}><h1 className={'esplix-name'}>ESPLIX</h1></div>
                </Segment>

                <Segment textAlign={'right'} style={{borderLeft: "0px"}}>
                    <Dropdown
                        text={t('Language')}
                        options={languageOptions}
                        selection
                        onChange={(e: SyntheticEvent<HTMLInputElement>, data: DropdownProps) => translator.setLanguage(data.value as string)}
                    />
                </Segment>
            </Segment.Group>
        </div>
    }
}

export default TopBar;