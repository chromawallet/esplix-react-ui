import * as React from 'react';

import { ParameterInfo, ActionInfo, RType } from "esplix/types/index";
import {ContractInstance, EsplixContext} from "esplix";

import { Customizer, CustomizedContractView } from "../../customizer";
import { Helper} from "../../defedit";

import ParameterArray from './ParameterArray';
import ParameterPK from './ParameterPK';
import ParameterPrimitive from './ParameterPrimitive';
import ParameterFileData from "./ParameterFileData";
import ParameterFileHash from "./ParameterFileHash";
import ParameterChainLink from "./ParameterChainLink";


// Field of form (with name and value)
//
// Parents:
// FormParameters - form for setting parameters
//
// Children:
// ParameterArray - array field of form
// ParameterChainLink - chainlink field of form
// ParameterFileData - upload file (data) field of form
// ParameterFileHash - upload file (hash) field of form
// ParameterPK - PUBKEY field of form
// ParameterPrimitive - primitive field of form
//
// Methods:
//
// Props
export interface ParameterProps {
    paramInfo: ParameterInfo; // Current parameter
    action: ActionInfo | null; // Current action
    contract: ContractInstance | null; // Current contract
    setParameterValue: (key: string, value: any) => null; // Function of setting parameter value
    customizer: Customizer; // Object with settings and service functions for app
    customizedContractView: CustomizedContractView | null; // Object with settings and service functions for contract view
    context: EsplixContext; // Esplix context of app
}

// State
export interface ParameterState {
}

export class Parameter extends React.Component<ParameterProps, ParameterState> {
    constructor(props: ParameterProps) {
        super(props);

        this.state = {};
    };

    typeHandlers = {
        "PUBKEY": ParameterPK,
        "FILEHASH": ParameterFileHash,
        "FILEDATA": ParameterFileData,
        "CHAINLINK": ParameterChainLink
    };

    render() {
        const tCustom = this.props.customizer.translate.getTCustom();
        const paramInfo: ParameterInfo = this.props.paramInfo;
        const action: ActionInfo | null = this.props.action;
        const context: EsplixContext = this.props.context;
        const contract: ContractInstance | null = this.props.contract;
        const type: any = paramInfo.type;
        const setParameterValue = this.props.setParameterValue;
        const customizer: Customizer = this.props.customizer;
        const customizedContractView: CustomizedContractView | null = this.props.customizedContractView;
        let name: string = "Parameter";

        if (customizedContractView && action)
            name = customizedContractView.getParameterName(action.name, paramInfo);
        else if (customizedContractView)
            name = customizedContractView.getParameterName(null, paramInfo);
        else
            name = paramInfo.name;

        // Translate name if available in translation file
        name = tCustom(name);

        let parameterInnerComponent;

        if (type.isPrimitive) {
            parameterInnerComponent = <ParameterPrimitive actionInfo={action} paramInfo={paramInfo} name={name} setParameterValue={setParameterValue} customizer={customizer} />;
        } else if (Helper.typeIsArray(type)) {
            parameterInnerComponent = <ParameterArray actionInfo={action} paramInfo={paramInfo} name={name} setParameterValue={setParameterValue} customizer={customizer} />;
        } else if (this.typeHandlers[type.name]) {
            const PTH = this.typeHandlers[type.name];

            parameterInnerComponent = <PTH actionInfo={action} contract={contract} context={context} paramInfo={paramInfo}
                name={name} setParameterValue={setParameterValue} customizer={customizer} />;
        } else {
            parameterInnerComponent = <p>"{name}": Parameter type {type.name} not supporter.</p>;
        }

        return parameterInnerComponent;
    }
}

export default Parameter;
