import * as React from 'react';

import { Dropdown, Form, Input, Icon } from 'semantic-ui-react';

import { ParameterInfo } from "esplix/types/index";
import {ContractInstance, EsplixContext} from "esplix";

import { AddressBook, AddressBookEntry, ParameterCompositeProps, Helper } from "../../defedit";
import {Customizer} from "../../customizer";

import PKValueDialog from './PKValueDialog';
import TextFieldDialog from '../dialogs/TextFieldDialog';


// PUBKEY field of form
//
// Parents:
// Parameter - field of form (with name and value)
//
// Children:
// TextFieldDialog - dialog with text field and OK and Close button
// PKValueDialog - select PUBKEY value dialog
// 
// Methods:
//
// Props
// ParameterCompositeProps
//
// State
export interface ParameterPKState {
    value: string;
    valueNew: string;
    mode: string;
}

export class ParameterPK extends React.Component<ParameterCompositeProps, ParameterPKState> {
    constructor(props: ParameterCompositeProps) {
        super(props);

        this.state = { value: "", valueNew: "", mode: "list" };
    };

    selectPKValue(e: Event, data: any) {
        e.preventDefault();

        const paramInfo: ParameterInfo = this.props.paramInfo;
        const setParameterValue = this.props.setParameterValue;
        const value: string = data.value;
        
        if (value === "__edit") {
            this.setState({ value: this.state.value, mode: "list", valueNew: "" });

            this.showAddEdit(value);

            //this.setState({ mode: "edit", valueNew: this.state.value });
        }
        else if (value === "__add") {
            this.setState({ value: this.state.value, mode: "list", valueNew: "" });

            this.showAddEdit(value);

            //this.setState({ mode: "add", valueNew: "" });
        }
        else if (value === "__lookup") {
            this.setState({ value: this.state.value, mode: "list", valueNew: "" });

            this.showLookup();

            //this.setState({ mode: "lookup", valueNew: "" });
        }
        else {
            setParameterValue(paramInfo.name, value.replace("__empty", ""));

            this.setState({ value: value, mode: "list", valueNew: "" });
        }
    };

    changeNewValue(e: Event, data: any) {
        e.preventDefault();

        const value: string = data.value;

        this.setState({ valueNew: data.value });
    };

    setValueManually(e: Event) {
        e.preventDefault();

        const mode = this.state.mode;
        const valueNew: string = this.state.valueNew;

        if (mode === "lookup" && valueNew)
            this.lookup();
        else if ((mode === "add" || mode === "edit") && valueNew)
            this.addEdit();
    };

    showAddEdit(mode: string) {
        const value = this.state.value;
        const showAlert = this.props.customizer.alert;
        
        if (mode ==="__add")
            showAlert(<TextFieldDialog header="Add new public key" fieldname="Public key value" defaultValue="" okHandler={this.addEditNew.bind(this)} showAlert={showAlert} />);
        else if (mode ==="__edit")
            showAlert(<TextFieldDialog header="Change public key" fieldname="Public key value" defaultValue={value} okHandler={this.addEditNew.bind(this)} showAlert={showAlert} />);
    };

    addEditNew(value: string) {
        const paramInfo: ParameterInfo = this.props.paramInfo;
        const customizer: Customizer = this.props.customizer;
        const addressBook: AddressBook = customizer.addressBook;
        const newAddress: AddressBookEntry | null = addressBook.searchAddressByPubkey(value);
        const address: AddressBookEntry = { name: value, pubkey: value };
        const setParameterValue = this.props.setParameterValue;
        const showAlert = this.props.customizer.alert;

        if (Helper.valueIsPubkey(value) === false) {
            showAlert("Public key value is incorrect");

            return;
        }
        
        if (newAddress === null && Helper.valueIsPubkey(value)===true)
            addressBook.addAddress(address);

        this.setState({ value: value, mode: "list", valueNew: "" });

        setParameterValue(paramInfo.name, value);
    };

    addEdit() {
        const paramInfo: ParameterInfo = this.props.paramInfo;
        const customizer: Customizer = this.props.customizer;
        const addressBook: AddressBook = customizer.addressBook;
        const valueNew: any = this.state.valueNew;
        const newAddress: AddressBookEntry | null = addressBook.searchAddressByPubkey(valueNew);
        const address: AddressBookEntry = { name: valueNew, pubkey: valueNew };
        const setParameterValue = this.props.setParameterValue;
        const showAlert = this.props.customizer.alert;

        if (Helper.valueIsPubkey(valueNew) === false) {
            showAlert("Public key value is incorrect");

            return;
        }
        
        if (newAddress === null && Helper.valueIsPubkey(valueNew)===true)
            addressBook.addAddress(address);

        this.setState({ value: valueNew, mode: "list", valueNew: "" });

        setParameterValue(paramInfo.name, valueNew);
    };

    showLookup() {
        //const id: string|null = prompt("ID to LookUp");

        //if (id)
        //    this.lookupNew(id);

        const showAlert = this.props.customizer.alert;

        showAlert(<TextFieldDialog header="LookUp" fieldname="ID to LookUp" defaultValue="" okHandler={this.lookupNew.bind(this)} showAlert={showAlert} />);
    };

    async lookupNew(id: string) {
        const paramInfo: ParameterInfo = this.props.paramInfo;
        const context: EsplixContext = this.props.context;
        const customizer: Customizer = this.props.customizer;
        const certificates = await customizer.lookup(id, context);
        const setParameterValue = this.props.setParameterValue;

        if (certificates.length === 0)
            return;

        this.setState({ value: certificates[0].pubkey, mode: "list", valueNew: "" });

        setParameterValue(paramInfo.name, certificates[0].pubkey);
    }

    async lookup() {
        const paramInfo: ParameterInfo = this.props.paramInfo;
        const context: EsplixContext = this.props.context;
        const customizer: Customizer = this.props.customizer;
        const valueNew: string = this.state.valueNew;
        const certificates = await customizer.lookup(valueNew, context);
        const setParameterValue = this.props.setParameterValue;

        if (certificates.length === 0)
            return;

        this.setState({ value: certificates[0].pubkey, mode: "list", valueNew: "" });

        setParameterValue(paramInfo.name, certificates[0].pubkey);
    }

    showList() {
        this.setState({ mode: "list", valueNew: "" });
    };

    render() {
        const customizer: Customizer = this.props.customizer;
        const addressBook = customizer.addressBook;
        const valueList: AddressBookEntry[] = this.props.customizer.addressBook.getAddresses();
        const name: string = this.props.name;
        const value: string = this.state.value;
        const valueNew: string = this.state.valueNew;
        let valueText: string = (value.replace("__empty", "")) ? Helper.resolvePubkey(Buffer.from(value, 'hex'), addressBook) : "";
        const mode = this.state.mode;
        let placeHolder = "";
        let options = [{ key: "__empty", value: "__empty", text: "Empty public key", icon: "" }];

        for (let i = 0; i < valueList.length; i++)
            options.push({ key: i.toString(), value: valueList[i].pubkey, text: valueList[i].name, icon: "" });

        if (value.replace("__empty", ""))
            options.push({ key: "__edit", value: "__edit", text: "Change public key " + valueText, icon: "edit" });

        options.push({ key: "__add", value: "__add", text: "Add new", icon: "add" }, { key: "__lookup", value: "__lookup", text: "LookUp", icon: 'search' });

        if (mode === "lookup")
            placeHolder = "ID to LookUp";
        else if (mode === "add" || mode === "edit")
            placeHolder = "Public key value";

        return (
            <Form.Field>
                <label>{name}</label>
                {mode === "list" &&
                    <Dropdown selection fluid options={options} value={value} onChange={this.selectPKValue.bind(this)} />
                }
                {mode !== "list" &&
                    <div>
                    <Input placeholder={placeHolder} onChange={this.changeNewValue.bind(this)} value={valueNew} style={{ width: "calc(100% - 65px)", marginRight: "5px" }} />
                        <Icon name="save" link size="large" onClick={this.setValueManually.bind(this)} />
                        <Icon name="close" link size="large" onClick={this.showList.bind(this)} />
                    </div>
                }
            </Form.Field>
        );
    }
}

export default ParameterPK;
