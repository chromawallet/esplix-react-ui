import * as React from 'react';

import { Header, TextArea, Button, Divider } from 'semantic-ui-react';

import { Customizer } from "../../customizer";


// Set array value dialog
//
// Parents:
// ParameterArray - array field of form
//
// Children:
// 
// Methods:
//
// Props
export interface ArrayValueDialogProps {
    currentValue: string[]; // Current value of field
    setArrayValue: (newValue: any) => null; // Function of setting value
    customizer: Customizer; // Object with settings and service functions for app
}

// State
export interface ArrayValueDialogState {
    selectedArrayValue: string;
}

export class ArrayValueDialog extends React.Component<ArrayValueDialogProps, ArrayValueDialogState> {
    constructor(props: ArrayValueDialogProps) {
        super(props);

        const currentValue: string[] = props.currentValue;
        let selectedArrayValue: string = "";

        for (let i = 0; i < currentValue.length; i++)
            selectedArrayValue += ((i !== 0) ? "\n" : "") + currentValue[i];
        
        this.state = { selectedArrayValue: selectedArrayValue };
    };

    selectArrayValue(e: Event, data: any) {
        e.preventDefault();

        this.setState({ selectedArrayValue: data.value });
    };

    setArrayValue(e: Event) {
        e.preventDefault();

        const setArrayValue: any = this.props.setArrayValue;
        const selectedArrayValue: string = this.state.selectedArrayValue;
        let values: string[] = selectedArrayValue.split("\n");

        setArrayValue(values);
    };

    closeDialog(e: MouseEvent) {
        e.preventDefault();

        const showAlert = this.props.customizer.alert;

        showAlert(null);
    };

    render() {
        const selectedArrayValue: any = this.state.selectedArrayValue;
        
        return (
            <form onSubmit={this.setArrayValue.bind(this)}>
                <div className="ui tiny modal transition visible active" style={{ marginTop: "-255px", overflow: "visible" }}>
                    <Header as='h3'>Set values:</Header>
                    <div style={{ padding: "20px 20px 5px 20px" }} className="content">
                        <TextArea placeholder='Set array values' rows="10" value={selectedArrayValue} onChange={this.selectArrayValue.bind(this)} style={{ width: "100%" }} />
                    </div>
                    <Divider />
                    <div style={{ textAlign: "right", padding: "0px 20px 0px 20px" }}>
                        <Button type="submit" primary>Select</Button>
                        &nbsp;
                        <Button secondary onClick={this.closeDialog.bind(this)}>Close</Button>
                    </div>
                </div>
            </form>
        )
	}
}

export default ArrayValueDialog;
