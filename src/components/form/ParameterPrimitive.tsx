import * as React from 'react';

import { Input, Form } from 'semantic-ui-react';

import { ParameterInfo } from "esplix/types/index";

import { ParameterSimpleProps } from "../../defedit";
import { Customizer} from "../../customizer";


// Primitive field of form
//
// Parents:
// Parameter - field of form (with name and value)
//
// Children:
// 
// Methods:
//
// Props
// ParameterSimpleProps
//
// State
export interface ParameterPrimitiveState {
    value: string;
}

export class ParameterPrimitive extends React.Component<ParameterSimpleProps, ParameterPrimitiveState> {
    constructor(props: ParameterSimpleProps) {
        super(props);

        this.state = { value: "" };
    };

    setValue(e: KeyboardEvent) {
        e.preventDefault();

        const paramInfo: ParameterInfo = this.props.paramInfo;
        const value: any = (e.target as HTMLInputElement).value;
        const setParameterValue = this.props.setParameterValue;

        this.setState({ value: value });

        let actualValue: any;

        switch (paramInfo.type.name) {
            case "INTEGER":
                actualValue = parseInt(value);
                break;
            default:
                actualValue = value;
        }

        setParameterValue(paramInfo.name, actualValue);
    };

    render() {
        const name: string = this.props.name;
        const value: string = this.state.value;

        return <Form.Input label={name} onChange={this.setValue.bind(this)} className="parameter-primitive" value={value} />;
    }
}

export default ParameterPrimitive;
