import * as React from 'react';

import { Header, Dropdown, Input, Button, Divider, Label } from 'semantic-ui-react';

import { AddressBook, AddressBookEntry } from "../../defedit";
import { Customizer} from "../../customizer";
import {EsplixContext} from "esplix";


// Select PUBKEY value dialog
//
// Parents:
// ParameterPK - PUBKEY field of form
//
// Children:
// 
// Methods:
//
// Props
export interface PKValueDialogProps {
    currentValue: any; // Current value of field
    setPKValue: (newValue: any) => null; // Function of setting value
    customizer: Customizer; // Object with settings and service functions for app
    context: EsplixContext; // Esplix context of app
}

// State
export interface PKValueDialogState {
    selectedPKValue: any;
    lookUpValue: string;
    lookUpMessage: string;
    isShowSelect: boolean;
}

export class PKValueDialog extends React.Component<PKValueDialogProps, PKValueDialogState> {
    constructor(props: PKValueDialogProps) {
        super(props);

        const currentValue: any = props.currentValue;

        this.state = { selectedPKValue: currentValue, lookUpValue: "", lookUpMessage: "", isShowSelect: true };
    };

    selectPKValue(e: Event, data: any) {
        e.preventDefault();

        this.setState({ selectedPKValue: data.value });
    };

    setLookUpValue(e: KeyboardEvent) {
        e.preventDefault();

        const value: any = (e.target as HTMLInputElement).value;

        this.setState({ lookUpValue: value, lookUpMessage: "" });
    };

    showLookUp(e: Event) {
        e.preventDefault();

        this.setState({ isShowSelect: false });
    };

    showSelect(e: Event) {
        e.preventDefault();

        this.setState({ isShowSelect: true });
    };

    closeDialog(e: MouseEvent) {
        e.preventDefault();

        const showAlert = this.props.customizer.alert;

        showAlert(null);
    };

    setPKValue(e: Event) {
        e.preventDefault();

        const setPKValue: any = this.props.setPKValue;
        const selectedPKValue: any = this.state.selectedPKValue;
        const isShowSelect: boolean = this.state.isShowSelect;

        if (isShowSelect===true)
            setPKValue(selectedPKValue);
    };

    async lookUp(e: Event) {
        e.preventDefault();

        const addressBook = this.props.customizer.addressBook;
        const context: EsplixContext = this.props.context;
        const customizer: Customizer = this.props.customizer;
        const id = customizer.lookupIDTransform(this.state.lookUpValue);
        const certificates = await context.certificateStore.getCertificates(id!);
        const showAlert = customizer.alert;

        console.log(certificates);

        if (certificates.length === 0) {
            this.setState({lookUpMessage: "No certificates were found"});
            addressBook.addAddress({name: this.state.lookUpValue, pubkey: this.state.lookUpValue })
            this.props.customizer.alert("Danger, arbitrary adding identities");
            return;
        }

        addressBook.addAddress(certificates[0]);

        // note: setState only after addressBook is updated.
        if (certificates.length > 1)
            this.setState({lookUpMessage: "Several certificates with this ID were found, using a random one"});
        else
            this.setState({isShowSelect: true});
    }

    render() {
        const t: Function = this.props.customizer.translate.t;
        const valueList: AddressBookEntry[] = this.props.customizer.addressBook.getAddresses();
        const selectedPKValue: any = this.state.selectedPKValue;
        const lookUpValue: string = this.state.lookUpValue;
        const lookUpMessage: string = this.state.lookUpMessage;
        const isShowSelect: boolean = this.state.isShowSelect;
        let options = [{ key: "", value: "", text: ""}];

        for (let i = 0; i < valueList.length; i++)
            options.push({ key: valueList[i].pubkey, value: valueList[i].pubkey, text: valueList[i].name });
        
        return (
            <form onSubmit={this.setPKValue.bind(this)}>
                <div className="ui tiny modal transition visible active" style={{ marginTop: "-255px", overflow: "visible" }}>
                    <Header as='h3'>{t('Select value')}:</Header>
                    <div style={{ padding: "20px 20px 5px 20px" }} className="content">
                        {isShowSelect===true &&
                            <Dropdown placeholder='Select PublicKey value' selection fluid options={options} value={selectedPKValue} onChange={this.selectPKValue.bind(this)} />
                        }
                        {isShowSelect === false &&
                            <div>
                                <Input label="ID to LookUp" onChange={this.setLookUpValue.bind(this)} value={lookUpValue} />
                                <Button icon='search' onClick={this.lookUp.bind(this)} />
                            </div>
                        }
                        {isShowSelect === false && lookUpMessage &&
                            <Label basic color='red' pointing>{lookUpMessage}</Label>
                        }
                        {!lookUpMessage &&
                            <div style={{ height: "25px" }}></div>
                        }
                    </div>
                    <Divider />
                    <div style={{ textAlign: "right", padding: "0px 20px 0px 20px" }}>
                        {isShowSelect === true &&
                            <Button secondary onClick={this.showLookUp.bind(this)}>Look up</Button>
                        }
                        {isShowSelect === false &&
                            <Button secondary onClick={this.showSelect.bind(this)}>Select</Button>
                        }
                        &nbsp;
                        <Button type="submit" primary>Set</Button>
                        &nbsp;
                        <Button secondary onClick={this.closeDialog.bind(this)}>Close</Button>
                    </div>
                </div>
            </form>
        )
	}
}

export default PKValueDialog;
