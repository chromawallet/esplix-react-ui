import * as React from 'react';

import { Header, Button, Form } from 'semantic-ui-react';

import { ParameterInfo, Parameters, ActionInfo, RType } from "esplix/types/index";

import { Customizer, CustomizedContractView } from "../../customizer";
import { Helper} from "../../defedit";

import Parameter from './Parameter';
import {ContractInstance, EsplixContext} from "esplix";


// Form for setting parameters
//
// Parents:
// ContractNew - create contract form
// ActionCard - action form
//
// Children:
// Parameter - field of form (with name and value)
//
// Methods:
//
// Props
export interface FormParametersProps {
    paramInfos: ParameterInfo[]; // List of parameters
    action: ActionInfo | null; // Current action
    contract: ContractInstance | null; // Current contract
    actionEnabled: boolean; // Attribute - action is enable or not
    formSubmit: (params: Parameters) => void; // Function of submitting form
    customizer: Customizer; // Object with settings and service functions for app
    customizedContractView: CustomizedContractView | null; // Object with settings and service functions for contract view
    context: EsplixContext; // Esplix context of app
}

// State
export interface FormParametersState {
    params: Parameters;
    performing: boolean;
}

export class FormParameters extends React.Component<FormParametersProps, FormParametersState > {
    constructor(props: FormParametersProps) {
        super(props);

        this.state = { params: {}, performing: false };
    };

    setParameterValue(key: string, value: any) {
        const params: Parameters = this.state.params;

        if (key)
            params[key] = value;

        this.setState({ params: params });
    };

    formSubmit(e: MouseEvent) {
        e.preventDefault();

        const params: any = this.state.params;
        const formSubmit = this.props.formSubmit;
        this.setState({performing: true});

        formSubmit(params);
    };

    componentDidUpdate(prevProps: FormParametersProps) {
        if (prevProps.action !== this.props.action) {
            this.setState({params: {}, performing: false})
        }
    }

    render() {
        const tCustom = this.props.customizer.translate.getTCustom();
        const action: ActionInfo | null = this.props.action;
        const paramInfos: ParameterInfo[] = this.props.paramInfos;
        const context: EsplixContext = this.props.context;
        const contract: ContractInstance | null = this.props.contract;
        const customizer: Customizer = this.props.customizer;
        const customizedContractView: CustomizedContractView | null = this.props.customizedContractView;
        let button: string = "Submit";
        let type: RType;

        if (customizedContractView && action) {
            button = customizedContractView.getActionButtonCaption(action.name);
        }
        else if (customizedContractView) {
            button = customizedContractView.getActionButtonCaption(null);
        }
        else if (action)
            button = action.name;

        return (
            <Form onSubmit={this.formSubmit.bind(this)}>
                {paramInfos.map((paramInfo, index) =>
                    <Parameter contract={contract} context={context} paramInfo={paramInfo} action={action} setParameterValue={this.setParameterValue.bind(this)}
                        customizer={customizer} customizedContractView={customizedContractView} key={index} />
                )}
                <Button disabled={!this.props.actionEnabled} primary loading={this.state.performing} type="submit">{tCustom(button)}</Button>
            </Form>
        );
    }
}

export default FormParameters;
