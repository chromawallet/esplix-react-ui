import * as React from 'react';
import {SyntheticEvent} from "react";

import {DropdownProps, Dropdown} from 'semantic-ui-react';

import { ParameterInfo, Parameters } from "esplix/types/index";
import {ContractInstance, EsplixContext} from "esplix";

import { ParameterCompositeProps } from "../../defedit";
import { Customizer} from "../../customizer";


// Chainlink field of form
//
// Parents:
// Parameter - field of form (with name and value)
//
// Children:
// 
// Methods:
//
// Props
// ParameterCompositeProps
//
// State
export interface ParameterChainLinkState {
    value: string;
    options: {text: string, value: string}[];
}

export class ParameterChainLink extends React.Component<ParameterCompositeProps, ParameterChainLinkState> {

    constructor(props: ParameterCompositeProps) {
        super(props);

        const context: EsplixContext = this.props.context;
        const contract: ContractInstance | null = this.props.contract;
        const options = this.props.customizer.getRelatedContractInstances(context, contract).map(
            item => {
                return {value: item.getChainID(), text: item.getChainID()}
            }
        );

        this.state = { value: "", options};
    };

    handleChange = (e: SyntheticEvent<HTMLElement>, data: DropdownProps) => {
        const myVal: string = data.value as string;

        this.changeDropdownSelection(myVal);
    };

    handleAddition = (e: SyntheticEvent<HTMLElement>, data: DropdownProps) => {
        const myVal: string = data.value as string;

        this.setState({
            options: [{ text: myVal, value: myVal }, ...this.state.options],
        });

        this.changeDropdownSelection(myVal);
    };

    changeDropdownSelection(myVal:string) {
        const setParameterValue = this.props.setParameterValue;

        this.setState({ value: myVal });

        setParameterValue(this.props.paramInfo.name, myVal);
    }

    render() {
        const name: string = this.props.name;
        const value: string = this.state.value;
        const options: { text: string, value: string }[] = this.state.options;

        return (
            <Dropdown header={name} options={options} search selection fluid allowAdditions value={value} style={{marginBottom: "14px"}}
                onAddItem={this.handleAddition.bind(this)} onChange={this.handleChange.bind(this)} />
        )
    }
}

export default ParameterChainLink;
