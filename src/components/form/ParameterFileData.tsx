import * as React from 'react';
import {SyntheticEvent} from "react";

import {Form, Input, InputOnChangeData, Icon, Segment} from 'semantic-ui-react';

import { ParameterCompositeProps } from "../../defedit";
import { Customizer } from "../../customizer";
import { ContractInstance, EsplixContext, ActionInfo } from "esplix";

// Upload file (data) field of form
//
// Parents:
// Parameter - field of form (with name and value)
//
// Children:
// 
// Methods:
//
// Props
// ParameterCompositeProps
//
// State
export interface ParameterFileDataState {
    fileName: string;
    isLoading: boolean;
}

export class ParameterFileData extends React.Component<ParameterCompositeProps, ParameterFileDataState> {

    useDigest: boolean;

    constructor(props: ParameterCompositeProps) {
        super(props);

        this.state = { fileName: "", isLoading: false };

        this.useDigest = false;


        const name: string = props.name;

        this.setValue = (e: SyntheticEvent<HTMLInputElement>, d: InputOnChangeData) => {

            if (d.value !== null){
                const actionInfo: ActionInfo | null = this.props.actionInfo;
                const file = (e.target as any).files[0] as File;
                const reader = new FileReader();
                const setParameterValue = this.props.setParameterValue;
                const contract: ContractInstance | null = this.props.contract;
                const context: EsplixContext = this.props.context;
                const customizer: Customizer = this.props.customizer;
                const showAlert = customizer.alert;

                if (file && file.name)
                    this.setState({ isLoading: true, fileName: file.name });
                else
                    this.setState({ isLoading: true });

                reader.onload = (e) => {
                    try {
                        const fileData = Buffer.from(reader.result);
                        const digest = context.getCryptoSystem().digest(fileData);

                        let promise = Promise.resolve();

                        //if (this.useDigest) {
                        //    promise = customizer.fileManager!.uploadFile(
                        //        context, contract, actionInfo!, {
                        //            file,
                        //            hash: digest,
                        //            contents: fileData
                        //        }
                        //    ).catch(
                        //        (err) => {
                        //            showAlert("Error uploading file");

                        //            this.setState({ isLoading: false });
                        //        }
                        //    )
                        //}

                        promise.then(
                            () => {
                                this.setState({ isLoading: false });
                                
                                setParameterValue(props.paramInfo.name, ((this.useDigest) ? digest : fileData).toString('hex'));
                            }
                        );
                    }
                    catch (e) {
                        showAlert("Error uploading file");

                        this.setState({ isLoading: false });
                    }
                };

                reader.readAsArrayBuffer(file);
            }
        };
    }

    setValue: (e: SyntheticEvent<HTMLInputElement>, d: InputOnChangeData) => void;
    
    render() {
        const name = this.props.name;
        const fileName: string = (this.state.fileName) ? "Selected file: " + this.state.fileName : "Click to select file";
        const isLoading: boolean = this.state.isLoading;

        return (
            <Form.Field>
                <label>333</label>
                <Segment style={{ paddingTop: "0px", paddingBottom: "9px", marginTop: "0px", marginBottom: "14px", boxShadow: "none", cursor: "pointer" }}>
                    <label className="custom-file-upload" style={{cursor: "pointer "}}>
                        <Form.Input type={"file"} onChange={this.setValue.bind(this)} style={{display: "none"}} />
                        {isLoading===true
                            ? <Icon loading={true} name='spinner' size="large" />
                            : <Icon name='file text' size="large" />
                        }
                        {fileName}
                    </label>
                </Segment>
            </Form.Field>
        )
    }
}

export default ParameterFileData;

