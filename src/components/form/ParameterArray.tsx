import * as React from 'react';

import { Form, TextArea, Icon } from 'semantic-ui-react';

import { ParameterInfo } from "esplix";

import { ParameterSimpleProps } from "../../defedit";
import { Customizer} from "../../customizer";

import ArrayValueDialog from './ArrayValueDialog';


// Array field of form
//
// Parents:
// Parameter - field of form (with name and value)
//
// Children:
// ArrayValueDialog - set array value dialog
//
// Methods:
//
// Props
// ParameterSimpleProps
//
// State
export interface ParameterArrayState {
    value: string;
    isSmallView: boolean;
    isEditMode: boolean;
}

export class ParameterArray extends React.Component<ParameterSimpleProps, ParameterArrayState> {
    constructor(props: ParameterSimpleProps) {
        super(props);

        this.state = { value: "", isSmallView: true, isEditMode: false };
    };

    setValue(e: KeyboardEvent, data: any) {
        e.preventDefault();
        
        const value: any = data.value;

        this.setState({ value: value });
    };

    confirmValue(e: KeyboardEvent) {
        e.preventDefault();

        const paramInfo: ParameterInfo = this.props.paramInfo;
        const setParameterValue = this.props.setParameterValue;
        let value: any = this.state.value;
        const customizer: Customizer = this.props.customizer;

        try {
            value = JSON.parse("[" + value + "]");

            this.setState({ isEditMode: false });

            setParameterValue(paramInfo.name, value);
        }
        catch (ex) {
            customizer.alert("Error: entered incorrect data");
        }        
    };

    setSmallView(isSmallView: boolean, e: Event) {
        e.preventDefault();

        this.setState({ isSmallView: isSmallView });
    };

    setEditMode(isEditMode: boolean, e: Event) {
        e.preventDefault();

        this.setState({ isEditMode: isEditMode });
    };

    render() {
        const name: string = this.props.name;
        const value: string = this.state.value;
        const isSmallView: boolean = this.state.isSmallView;
        const isEditMode: boolean = this.state.isEditMode;

        return (
            <Form.Field>
                <label>
                    <div style={{ display: "inline", paddingRight: "15px" }}>{name}</div>
                    {isSmallView === false &&
                        <Icon link name="chevron up" size="small" onClick={this.setSmallView.bind(this, true)} />
                    }
                    {isSmallView === true &&
                        <Icon link name="chevron down" size="small" onClick={this.setSmallView.bind(this, false)} />
                    }
                </label>
                {isSmallView === true && isEditMode === false &&
                    <Form.Input onClick={this.setEditMode.bind(this, true)} className="parameter-primitive" value={value} />
                }
                {isSmallView === true && isEditMode === true &&
                    <Form.Input onChange={this.setValue.bind(this)} className="parameter-primitive" value={value} focus
                        icon={<Icon name="save" link size="large" onClick={this.confirmValue.bind(this)} />} />
                }
                {isSmallView === false && isEditMode === false &&
                    <TextArea rows="5" value={value} onClick={this.setEditMode.bind(this, true)} style={{ width: "100%" }} />
                }
                {isSmallView === false && isEditMode === true &&
                    <div>
                    <TextArea rows="5" value={value} onChange={this.setValue.bind(this)} style={{ width: "calc(100% - 35px)", marginRight: "5px" }} />
                        <Icon name="save" link size="large" onClick={this.confirmValue.bind(this)} />
                    </div>
                }
                </Form.Field>
        );
    }
}

export default ParameterArray;
