import {dummyConfig, postchainConfig, EsplixContext} from "esplix";

const contracts: string[] = [
    require("./test123.r4o.json"),
    require("./demov5.r4o.json"),
    require("./nsw009.r4o.json")
    // require("./canntest3.r4o.json")
];
    //require("./demov5.r4o.json"),
    //require("./dXX2.r4o.json"),
    //require("./dXX3.r4o.json"),
    //require("./futuristic.ratc.r4o.json")
    //require("./demov7.r4o.json"),
    //require("./canntest3.r4o.json")


export async function dummyContext() {
    const context = new EsplixContext(dummyConfig());
    await context.initialize();

    const cdfes = contracts.map(
        ctr => context.contractDefinitionManager.registerDefinition(Buffer.from(ctr, 'hex'))
    );

    /*await context.contractInstanceManager.createContractInstance(contractDefinition,
        { SELLER: context.principalIdentity.getPublicKey()});*/

    return { context, contractDefinition: cdfes[0] }
}

export async function initializedDummyContext() {
    const { context, contractDefinition } = await dummyContext();
    await context.principalIdentity.generateIdentity();
    await context.principalIdentity.setAttribute('role', 'user');
    return { context, contractDefinition };
}

export async function postchainContext() {
    const config = postchainConfig("http://35.204.193.71:5001/",
        "http://messaging.esplix1.chromaway.net",
        Buffer.from("ae56ed7dc4fb49e6162c7d9ecbb13684928b81f0464d675b23547963700874c7", 'hex'));
    const context = new EsplixContext(config);
    await context.initialize();
    const cdfes = contracts.map(
        ctr => context.contractDefinitionManager.registerDefinition(Buffer.from(ctr, 'hex'))
    );
    return { context, contractDefinition: cdfes[0] }
}

