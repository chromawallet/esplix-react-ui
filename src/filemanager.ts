import {ActionInfo, ContractInstance, EsplixContext} from "esplix";
//import * as WebRequest from "web-request";

export interface FileInfo {
    file?: File;
    hash: Buffer;
    contents: Buffer;
}

export interface FileManager {
    uploadFile(context: EsplixContext, contractInstance: ContractInstance, actionInfo: ActionInfo, file: FileInfo): Promise<void>;
    downloadFile(context: EsplixContext, contractInstance: ContractInstance, actionInfo: ActionInfo, fileHash: Buffer): Promise<Buffer>;
}

export class DummyFileUploader implements FileManager {
    uploadFile(context: EsplixContext, contractInstance: ContractInstance, actionInfo: ActionInfo, file: FileInfo): Promise<void> {
        return Promise.resolve();
    }
    downloadFile(context: EsplixContext, contractInstance: ContractInstance, actionInfo: ActionInfo, fileHash: Buffer): Promise<Buffer> {
        return Promise.reject(Error("not implemented"))
    }

}
/*
export class PostFileUploader implements FileManager {
    url: string;

    constructor(url: string) {
        this.url = url;
    }

    uploadFile(context: EsplixContext, contractInstance: ContractInstance, actionInfo: ActionInfo, file: FileInfo): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            if (file.hash && file.contents) {
                WebRequest.post(this.url, undefined,
                    {
                        hash: file.hash,
                        contents: file.contents.toString('hex')
                    }).then(
                    result => resolve(),
                    error => reject()
                );
            }
            else {
                reject();
            }
        });
    }
}*/